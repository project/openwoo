## CONTENTS OF THIS FILE

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers


## INTRODUCTION

Dutch governmental organizations have to apply the WOO. This module provides a framework to publish
and search documents available in different content providers. These providers are pluggable.


## REQUIREMENTS

The base module requires no modules beside Drupal core.
The submodule openwoo_publish has the following dependencies:
* openwoo (the base module)
* media
* media_library


## INSTALLATION

Install this module like other Drupal modules.

1. If your site is [managed via Composer](https://www.drupal.org/node/2718229),
   use Composer to download the OpenWoo module running
   ```composer require "drupal/openwoo"```. Otherwise, copy/upload the OpenWoo
   module to the modules directory of your Drupal installation.
2. Enable the 'OpenWoo' module and desired sub-modules in 'Extend'.
   (`/admin/modules`)
3. Set up user permissions.
   (`/admin/people/permissions/module/openwoo` and optionally
   `/admin/people/permissions/module/openwoo_publish`)


## CONFIGURATION

- Set up the organization (`/admin/config/services/openwoo`).

### For the OpenWoo Search module

1. Enable the module.
2. Select the client you want to use (`/admin/config/services/openwoo/openwoo-search`)
3. If you use OpenWoo.app, add the api-key to settings.php
   ```$config['openwoo_search.openwoo_app']['api_key'] = 'API_KEY';```
4. Place OpenWoo search block (`/admin/structure/block`)

### For the OpenWoo Publish module

1. Enable the module.
2. Select the client you want to use (`/admin/config/services/openwoo/openwoo-publish`)
3. Select the correct endpoint. Consult the OpenWoo.app-team if in doubt.
4. If you use OpenWoo.app, add the api-key to settings.php
   ```$config['openwoo_publish.openwoo_app']['api_key'] = 'API_KEY';```
5. Go to `admin/content/openwoo/publications` to create a publication.
   It will be pushed to OpenWoo.app on the next cron run.

### For the OpenWoo Attachments module

1. Enable the module.
2. Add the s3fs settings to settings.php <br>
   `$settings['s3fs.access_key'] = 'YOUR ACCESS KEY';` <br>
   `$settings['s3fs.secret_key'] = 'YOUR SECRET KEY';` <br>
   `$config['s3fs.settings']['bucket'] = 'BUCKET NAME';` <br>
   `$config['s3fs.settings']['region'] = 'BUCKET REGION';` <br>
   `$settings['s3fs.use_s3_for_public'] = FALSE;` <br>
   `$settings['s3fs.upload_as_private'] = TRUE;`
3. Go to `admin/content/openwoo/publications` to create a publication with a attachment.
   The attachment will now be saved to your Bucket.

For further information se the s3fs module documentation: https://www.drupal.org/project/s3fs.

## MAINTAINERS

Current development is supported by:
- iO - https://www.drupal.org/io
