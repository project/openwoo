<?php

namespace Drupal\openwoo_attachments\Tests\Kernel;

use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests OpenWoo Attachments.
 *
 * @group openwoo
 */
class OpenWooAttachmentsTest extends KernelTestBase {
  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'user',
    'system',
    'openwoo',
    'openwoo_publish',
    'file',
    'system',
    'options',
    'user',
    'field',
    'media',
    'image',
    's3fs',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('media');
    $this->installEntitySchema('user');
    $this->installSchema('user', ['users_data']);
    $this->installConfig(['system', 'media']);
  }

  /**
   * Tests if uri_scheme is 's3' when openwoo_attachments is installed.
   */
  public function testOpenWooAttachmentsStorage(): void {
    // Create the field storage configuration.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_openwoo_media_file',
      'entity_type' => 'media',
      'type' => 'file',
      'settings' => [
        'uri_scheme' => 'public',
      ],
    ]);

    // Save the field storage configuration.
    $field_storage->save();

    // Install the openwoo_attachments module.
    \Drupal::service('module_installer')->install(['openwoo_attachments']);
    $field_storage = FieldStorageConfig::load('media.field_openwoo_media_file');
    // Check if the uri_scheme is set to 's3'.
    $this->assertEquals('s3', $field_storage->get('settings')['uri_scheme']);

    // Uninstall the openwoo_attachments module.
    \Drupal::service('module_installer')->uninstall(['openwoo_attachments']);
    $field_storage = FieldStorageConfig::load('media.field_openwoo_media_file');
    // Check if the uri_scheme is set to 'public'.
    $this->assertEquals('public', $field_storage->get('settings')['uri_scheme']);

  }

}
