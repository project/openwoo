<?php

namespace Drupal\openwoo_publish\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines OpenWooPublish annotation object.
 *
 * @Annotation
 */
class OpenWooPublish extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The configuration object name where the settings are stored.
   *
   * @var string
   */
  public $configName;

}
