<?php

namespace Drupal\openwoo_publish\Config;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorableConfigBase;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides configuration overrides to add File Resumable Upload.
 */
class FileResupConfig implements ConfigFactoryOverrideInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new FileResupConfig object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory) {
    $this->moduleHandler = $module_handler;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names): array {
    $overrides = [];

    // Add File Resumable Upload to media entity if the module is enabled.
    if ($this->moduleHandler->moduleExists('file_resup_media_library') && in_array('field.field.media.openwoo_attachment.field_openwoo_media_file', $names)) {
      $config = $this->configFactory->get('openwoo_publish.settings');
      $overrides['field.field.media.openwoo_attachment.field_openwoo_media_file'] = [
        'third_party_settings' => [
          'file_resup' => [
            'enabled' => $config->get('file_resup.enabled') ? 1 : 0,
            'auto_upload' => $config->get('file_resup.auto_upload') ? 1 : 0,
            'max_upload_size' => $config->get('file_resup.max_upload_size') ?: '',
          ],
        ],
      ];
    }

    return $overrides;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix(): string {
    return 'file_resup_config';
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION): ?StorableConfigBase {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name) {
    return new CacheableMetadata();
  }

}
