<?php

namespace Drupal\openwoo_publish\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the OpenWoo publication.
 *
 * @ingroup openwoo_publish
 *
 * @ContentEntityType(
 *   id = "openwoo_publication",
 *   label = @Translation("OpenWoo publication"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\openwoo_publish\OpenWooPublicationListBuilder",
 *     "access" = "Drupal\openwoo_publish\OpenWooPublicationAccessControlHandler",
 *     "permission_provider" = "Drupal\Core\Entity\EntityPermissionProvider",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\openwoo_publish\Form\OpenWooPublicationForm",
 *       "add" = "Drupal\openwoo_publish\Form\OpenWooPublicationForm",
 *       "edit" = "Drupal\openwoo_publish\Form\OpenWooPublicationForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "openwoo_publications",
 *   admin_permission = "administer openwoo publications",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/openwoo-publications/{openwoo_publication}",
 *     "add-form" = "/admin/content/openwoo/publications/add",
 *     "edit-form" = "/admin/content/openwoo/publications/{openwoo_publication}/edit",
 *     "delete-form" = "/admin/content/openwoo/publications/{openwoo_publication}/delete",
 *     "collection" = "/admin/content/openwoo/publications",
 *   },
 * )
 */
class OpenWooPublication extends ContentEntityBase implements OpenWooPublicationInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): ?int {
    return $this->get('created')->value ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp): OpenWooPublication {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime(): ?int {
    return $this->get('changed')->value ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setPublicationData(array $form_values): int {
    $values_to_serialize = [];
    $fields_to_skip = [
      'label',
      'attachments',
      'status',
    ];
    foreach ($form_values as $key => $value) {
      if (!in_array($key, $fields_to_skip)) {
        $values_to_serialize[$key] = $value;
      }
    }
    $this->set('data', serialize($values_to_serialize));
    return $this->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getPublicationData(): array {
    $publication_data = unserialize($this->get('data')->value, ['allowed_classes' => FALSE]);
    return !(empty($publication_data)) ? $publication_data : [];
  }

  /**
   * {@inheritdoc}
   */
  public function setExternalId(string $external_id): int {
    $this->set('external_id', $external_id);
    return $this->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalId(): string|null {
    return $this->get('external_id')->value ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Publication title'))
      ->setDescription(t('The title of the publication.'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ]);

    $fields['data'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Publication data'))
      ->setDescription(t('The serialized data for the publication.'))
      ->setSetting('case_sensitive', TRUE)
      ->setDefaultValue('')->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'openwoo_publish_data',
        'weight' => 10,
      ]);

    $fields['attachments'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Attachments'))
      ->setDescription(t('Add attachments to the publication.'))
      ->setRequired(FALSE)
      ->setSetting('target_type', 'media')
      ->setSetting('handler', 'default:media')
      ->setSettings([
        'handler_settings' => [
          'target_bundles' => [
            'openwoo_attachment' => 'openwoo_attachment',
          ],
          'sort' => [
            'field' => '_none',
          ],
          'auto_create' => FALSE,
          'auto_create_bundle' => '',
        ],
      ])
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', [
        'type' => 'media_library_widget',
        'weight' => 20,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'settings' => [
          'target_type' => 'media',
        ],
        'weight' => 20,
      ]);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Published'))
      ->setTranslatable(FALSE)
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 100,
      ]);

    $fields['external_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('External id'))
      ->setDescription(t('The external id.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the entity was last edited.'));

    return $fields;
  }

}
