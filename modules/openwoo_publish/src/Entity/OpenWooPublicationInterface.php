<?php

namespace Drupal\openwoo_publish\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Represents a OpenWoo publication.
 */
interface OpenWooPublicationInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface, EntityPublishedInterface {

  /**
   * Publication is not published.
   */
  public const NOT_PUBLISHED = 'Not published';

  /**
   * Publication is published.
   */
  public const PUBLISHED = 'Published';

  /**
   * Gets the OpenWoo publication creation timestamp.
   *
   * @return int|null
   *   The created time.
   */
  public function getCreatedTime(): ?int;

  /**
   * Sets the OpenWoo publication creation timestamp.
   *
   * @param int $timestamp
   *   The created time.
   *
   * @return \Drupal\openwoo_publish\Entity\OpenWooPublication
   *   The OpenWoo publication.
   */
  public function setCreatedTime($timestamp): OpenWooPublication;

  /**
   * Gets the OpenWoo publication modification timestamp.
   *
   * @return int|null
   *   The modified time.
   */
  public function getChangedTime(): ?int;

  /**
   * Sets the OpenWoo publication creation data.
   *
   * @param array $form_values
   *   An array with form values.
   *
   * @return int
   *   Whether the publication was successfully updated.
   */
  public function setPublicationData(array $form_values): int;

  /**
   * Gets the OpenWoo publication data.
   *
   * @return array
   *   An array with all publication data.
   */
  public function getPublicationData(): array;

  /**
   * Sets the OpenWoo external ID.
   *
   * @param string $external_id
   *   A string with the external ID.
   *
   * @return int
   *   Whether the publication was successfully updated.
   */
  public function setExternalId(string $external_id): int;

  /**
   * Gets the external ID of the publication.
   *
   * @return string|null
   *   The external ID of the publication or null.
   */
  public function getExternalId(): string|null;

}
