<?php

namespace Drupal\openwoo_publish\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\openwoo\OpenWooPublicationFields;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the OpenWoo Publish forms.
 *
 * @ingroup openwoo_publish
 */
class OpenWooPublicationForm extends ContentEntityForm {

  /**
   * Service with helpers for the form fields.
   *
   * @var \Drupal\openwoo\OpenWooPublicationFields
   */
  protected $publicationFieldsService;

  /**
   * Constructs a ContentEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\openwoo\OpenWooPublicationFields $publication_fields_service
   *   The OpenWooPublicationFields service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, OpenWooPublicationFields $publication_fields_service) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->publicationFieldsService = $publication_fields_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('openwoo.publication_fields')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form[] = $this->publicationFieldsService->getForm($this->entity->getPublicationData());

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    // If a field for a request type is filled, the title is required.
    foreach ($this->publicationFieldsService->getRequestFields() as $key => $value) {
      $title_required = FALSE;
      foreach ($value['fields'] as $field) {
        $title_required = !empty($form_state->getvalue($field)) ? TRUE : $title_required;
      }
      if ($title_required && empty($form_state->getvalue($key . '_title'))) {
        $form_state->setErrorByName(
          $key . '_title',
          t('%category title is required.', ['%category' => $value['label']]),
        );
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $entity = $this->entity;
    $form_values = $form_state->getValues();
    $status = $entity->setPublicationData($form_values);
    // Handle the result of the entity save and redirect the user.
    if ($status) {
      $this->messenger()->addMessage($this->t('Saved OpenWoo publication %label.', [
        '%label' => $entity->label(),
      ]));
    }
    else {
      $this->messenger()->addError($this->t('Could not save OpenWoo publication %label.', [
        '%label' => $entity->label(),
      ]));
    }
    $form_state->setRedirect('entity.openwoo_publication.canonical', ['openwoo_publication' => $entity->id()]);
  }

}
