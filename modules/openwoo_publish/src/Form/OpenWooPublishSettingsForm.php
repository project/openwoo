<?php

namespace Drupal\openwoo_publish\Form;

use Drupal\Component\Utility\Environment;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\ByteSizeMarkup;
use Drupal\openwoo_publish\OpenWooPublishPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OpenWooPublishSettingsForm. Config form for the OpenWoo Publish module.
 *
 * @package Drupal\openwoo\Form
 */
class OpenWooPublishSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'openwoo_publish.settings';

  /**
   * The OpenWooPublish' plugin manager.
   *
   * @var \Drupal\openwoo_publish\OpenWooPublishPluginManager
   */
  protected $openWooPublishPluginManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * SettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\openwoo_publish\OpenWooPublishPluginManager $openwoo_publish_plugin_manager
   *   The OpenWoo plugin manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, OpenWooPublishPluginManager $openwoo_publish_plugin_manager, ModuleHandlerInterface $module_handler) {
    parent::__construct($config_factory);
    $this->openWooPublishPluginManager = $openwoo_publish_plugin_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.openwoo_publish'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'openwoo_publish.settings',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'openwoo_publish_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(static::SETTINGS);

    $form['client'] = [
      '#type' => 'select',
      '#title' => $this->t('Client'),
      '#empty_option' => $this->t('- Select -'),
      '#options' => $this->openWooPublishPluginManager->getPlugins(),
      '#description' => $this->t('Choose the provider where we publish content to.'),
      '#default_value' => $config->get('client'),
      '#ajax' => [
        'callback' => '::updateClientSettings',
        'wrapper' => 'client-settings-wrapper',
      ],
    ];

    $form['extended_logging'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Extended logging'),
      '#default_value' => $config->get('extended_logging') ?? 0,
    ];

    $form['client_settings'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'client-settings-wrapper'],
    ];

    // Add the current client settings to the form.
    $client = $form_state->getValue('client') ?? $config->get('client');
    if ($client) {
      $form['client_settings'] += $this->openWooPublishPluginManager->getFormElements($client, $form, $form_state);
    }

    // Add File Resumable Upload settings if the module is enabled.
    if ($this->moduleHandler->moduleExists('file_resup_media_library')) {
      $form['file_resup'] = [
        '#type' => 'details',
        '#title' => $this->t('File Resumable Upload'),
        '#description' => $this->t('The File Resumable Upload module and it\'s media library integration are enabled. You can use it for better upload handling of large attachments for publications.'),
        '#open' => (bool) $config->get('file_resup.enabled'),
        '#tree' => TRUE,
        'enabled' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Use File Resumable Upload to upload attachments'),
          '#default_value' => $config->get('file_resup.enabled') ?? 0,
        ],
        'auto_upload' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Start upload on files added'),
          '#description' => $this->t('When checked, upload will start as soon as files are added without requiring to click Upload, unless some of the added files did not pass validation.'),
          '#default_value' => $config->get('file_resup.auto_upload') ?? 0,
        ],
        'max_upload_size' => [
          '#type' => 'textfield',
          '#title' => $this->t('Maximum upload size'),
          '#description' => $this->t('Enter a value like "512" (bytes), "80 KB" (kilobytes) or "50 MB" (megabytes) in order to restrict the allowed file size. If left empty the file sizes could be limited only by PHP\'s maximum post and file upload sizes (current limit <strong>%limit</strong>).', [
            '%limit' => ByteSizeMarkup::create(Environment::getUploadMaxSize()),
          ]),
          '#default_value' => $config->get('file_resup.max_upload_size') ?? NULL,
        ],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * AJAX callback to update the client settings form.
   *
   * @param array $form
   *   The form render array.
   *
   * @return array
   *   The form elements.
   */
  public function updateClientSettings(array $form): array {
    return $form['client_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('openwoo_publish.settings')
      ->set('client', $form_state->getValue('client'))
      ->set('extended_logging', $form_state->getValue('extended_logging'))
      ->save();
    $this->openWooPublishPluginManager->submitFormElements($form_state->getValue('client'), $form['client_settings'], $form_state);

    if ($form_state->hasValue('file_resup')) {
      $this->config('openwoo_publish.settings')
        ->set('file_resup.enabled', $form_state->getValue(['file_resup', 'enabled']))
        ->set('file_resup.auto_upload', $form_state->getValue(['file_resup', 'auto_upload']))
        ->set('file_resup.max_upload_size', $form_state->getValue(['file_resup', 'max_upload_size']))
        ->save();
    }

    parent::submitForm($form, $form_state);
  }

}
