<?php

namespace Drupal\openwoo_publish;

use Drupal\openwoo_publish\Entity\OpenWooPublication;

/**
 * Provides an interface for the OpenWOoPublishJobCreator.
 */
interface OpenWOoPublishJobCreatorInterface {

  /**
   * Add the OpenWooPublication to queue.
   *
   * @param \Drupal\openwoo_publish\Entity\OpenWooPublication $entity
   *   The OpenWooPublication entity.
   * @param string $action
   *   The OpenWooPublication entity.
   */
  public function createJobs(OpenWooPublication $entity, string $action);

}
