<?php

namespace Drupal\openwoo_publish;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for OpenWoo publications.
 */
class OpenWooPublicationAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view openwoo publications');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit openwoo publications');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete openwoo publications');
    }
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add openwoo publications');
  }

}
