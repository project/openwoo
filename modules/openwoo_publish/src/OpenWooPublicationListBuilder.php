<?php

namespace Drupal\openwoo_publish;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\openwoo_publish\Entity\OpenWooPublication;
use Drupal\openwoo_publish\Entity\OpenWooPublicationInterface;

/**
 * Provides a list controller for OpenWoo publications.
 *
 * @ingroup openwoo_publish
 */
class OpenWooPublicationListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Title');
    $header['status'] = $this->t('Status');
    $header['created'] = $this->t('Created');
    $header['changed'] = $this->t('Changed');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    assert($entity instanceof OpenWooPublication);
    $row['label'] = $entity->toLink($entity->label());
    $row['status'] = $entity->isPublished() ? OpenWooPublicationInterface::PUBLISHED : OpenWooPublicationInterface::NOT_PUBLISHED;
    $row['created'] = gmdate("d-m-Y - h:i", $entity->getCreatedTime());
    $row['changed'] = gmdate("d-m-Y - h:i", $entity->getChangedTime());
    return $row + parent::buildRow($entity);
  }

}
