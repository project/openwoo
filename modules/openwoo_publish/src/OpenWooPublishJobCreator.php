<?php

namespace Drupal\openwoo_publish;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\openwoo_publish\Entity\OpenWooPublication;

/**
 * Handles the cron for the OpenWoo Publish module.
 */
class OpenWooPublishJobCreator implements OpenWOoPublishJobCreatorInterface {
  use StringTranslationTrait;
  use LoggerChannelTrait;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The OpenWoo Publish job queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Database.
   *
   * @var \Drupal\Core\Database\Database
   * It acts to encapsulate all control
   */
  protected $database;

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructs a new OpenWooPublishJobCreator.
   *
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   System time.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   The queue factory.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(TimeInterface $time, QueueFactory $queueFactory, Connection $database, ConfigFactoryInterface $config_factory) {
    $this->time = $time;
    $this->queue = $queueFactory->get('openwoo_publish_job');
    $this->database = $database;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function createJobs(OpenWooPublication $entity, string $action) {
    $queueItem = [];
    $queueItem['publication_id'] = $entity->id();
    $queueItem['action'] = $action;
    $queueItem['external_id'] = $entity->getExternalId();
    $this->queue->createItem($queueItem);

    if ($this->extendedLoggingEnabled()) {
      $this->getLogger('openwoo_publish')->info('Queued OpenWooPublication #@id to be pushed to the webserver.', [
        '@id' => $entity->id(),
      ]);
    }
  }

  /**
   * Is extended logging enabled.
   *
   * @return bool
   *   Returns the extended logging value.
   */
  public function extendedLoggingEnabled(): bool {
    return $this->configFactory->get('openwoo_publish.settings')->get('extended_logging');
  }

}
