<?php

namespace Drupal\openwoo_publish;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * OpenWooPublish plugin manager.
 */
class OpenWooPublishPluginManager extends DefaultPluginManager implements OpenWooPublishPluginManagerInterface {
  use StringTranslationTrait;
  use LoggerChannelTrait;

  /**
   * List of all instances.
   *
   * @var array
   */
  protected $instances = [];

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructs OpenWooPublishPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory) {
    parent::__construct(
      'Plugin/OpenWooPublish',
      $namespaces,
      $module_handler,
      'Drupal\openwoo_publish\Plugin\OpenWooPublishPluginInterface',
      'Drupal\openwoo_publish\Annotation\OpenWooPublish'
    );
    $this->alterInfo('openwoo_publish_info');
    $this->setCacheBackend($cache_backend, 'openwoo_publish_plugins');
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugins(): array {
    $plugins = $this->getDefinitions();
    foreach ($plugins as $plugin_id => $plugin) {
      $plugins[$plugin_id] = $plugin['label']->render();
    }
    return $plugins;
  }

  /**
   * {@inheritdoc}
   */
  public function getActivePlugin(): ?object {
    if ($plugin_id = $this->configFactory->get('openwoo_publish.settings')->get('client')) {
      return $this->createInstance($plugin_id);
    }

    $this->getLogger('openwoo_publish')
      ->error('No publish plugin selected.');
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormElements($plugin_id, array &$form, FormStateInterface $form_state): array {
    $plugin = $this->createInstance($plugin_id);

    return $plugin->getElements();
  }

  /**
   * {@inheritdoc}
   */
  public function submitFormElements(string $plugin_id, array $form, FormStateInterface $form_state): void {
    $plugin = $this->createInstance($plugin_id);
    $plugin->submitElements($form, $form_state);
  }

}
