<?php

namespace Drupal\openwoo_publish\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\openwoo\OpenWooPublicationFields;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin to correctly format the data of a OpenWoo publication.
 *
 * @FieldFormatter(
 *   id = "openwoo_publish_data",
 *   label = @Translation("OpenWoo data object"),
 *   field_types = {
 *     "string_long"
 *   }
 * )
 */
class OpenWooDataFormatter extends FormatterBase {

  /**
   * Service with helpers for the form fields.
   *
   * @var \Drupal\openwoo\OpenWooPublicationFields
   */
  protected OpenWooPublicationFields $publicationFieldsService;

  /**
   * Constructs a OpenWooDataFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\openwoo\OpenWooPublicationFields $publication_fields_service
   *   The OpenWooPublicationFields service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, OpenWooPublicationFields $publication_fields_service) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->publicationFieldsService = $publication_fields_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('openwoo.publication_fields')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Correctly formats the serialized data for the OpenWoo publication.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $publication_data = $this->publicationFieldsService->getPublication($item->getEntity()->getPublicationData(), 'view');
      $element[$delta] = [
        '#theme' => 'openwoo_publish_publication',
        '#publication_data' => $publication_data,
      ];
    }

    return $element;
  }

}
