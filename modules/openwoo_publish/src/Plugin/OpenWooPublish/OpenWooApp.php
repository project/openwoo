<?php

namespace Drupal\openwoo_publish\Plugin\OpenWooPublish;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\media\Entity\Media;
use Drupal\openwoo\OpenWooPublicationFields;
use Drupal\openwoo_publish\OpenWooPublishPluginManager;
use Drupal\openwoo_publish\Plugin\OpenWooPublishPluginBase;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the openwoo_app.
 *
 * @OpenWooPublish(
 *   id = "openwoo_app",
 *   label = @Translation("OpenWoo.app"),
 *   description = @Translation("Publish OpenWoo publications to OpenWoo.app"),
 *   configName = "openwoo_publish.openwoo_app"
 * )
 */
class OpenWooApp extends OpenWooPublishPluginBase {
  use StringTranslationTrait;

  /**
   * Array with the publication data.
   *
   * @var array
   */
  protected array $publicationData;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * Constructs a Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The http client.
   * @param \Drupal\openwoo_publish\OpenWooPublishPluginManager $openwoo_publish_plugin_manager
   *   The OpenWoo plugin manager.
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager interface.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator.
   * @param \Drupal\openwoo\OpenWooPublicationFields $publication_fields_service
   *   The OpenWooPublicationFields service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactoryInterface $config_factory,
    ClientInterface $http_client,
    OpenWooPublishPluginManager $openwoo_publish_plugin_manager,
    DateFormatter $date_formatter,
    EntityTypeManagerInterface $entity_type_manager,
    FileUrlGeneratorInterface $file_url_generator,
    OpenWooPublicationFields $publication_fields_service,
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $config_factory,
      $http_client,
      $openwoo_publish_plugin_manager,
      $entity_type_manager,
      $file_url_generator,
      $publication_fields_service);
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('http_client'),
      $container->get('plugin.manager.openwoo_publish'),
      $container->get('date.formatter'),
      $container->get('entity_type.manager'),
      $container->get('file_url_generator'),
      $container->get('openwoo.publication_fields'),
    );
  }

  /**
   * Get the api url.
   *
   * @returns string
   *    The api url.
   */
  public function getApiUrl(): string {
    return $this->config()->get('api_url') . '/api/publicaties';
  }

  /**
   * Get the api key.
   *
   * @return string|null
   *   The api key.
   */
  public function getApiKey(): string|NULL {
    return $this->configFactory->get($this->pluginDefinition['configName'])->get('api_key');
  }

  /**
   * {@inheritDoc}
   */
  public function addPublication(EntityInterface $openwoo_publication): string|null {
    $body = $this->buildRequestBody($openwoo_publication);
    $headers = [];
    $options = $this->getOptions($body, $headers);
    $content = $this->sendRequest($this->getApiUrl(), $options, 'post');
    $decoded_content = (!empty($content)) ? json_decode($content) : NULL;
    if ($decoded_content === NULL) {
      return NULL;
    }
    $openwoo_publication->setExternalId($decoded_content->_id);

    return $content;
  }

  /**
   * {@inheritDoc}
   */
  public function updatePublication(EntityInterface $openwoo_publication, string $external_id): string|null {
    $body = $this->buildRequestBody($openwoo_publication);
    $headers = [];
    $options = $this->getOptions($body, $headers);
    $content = $this->sendRequest($this->getApiUrl() . '/' . $external_id, $options, 'put');
    $decoded_content = (!empty($content)) ? json_decode($content) : NULL;
    if ($decoded_content === NULL) {
      return NULL;
    }
    return $content;
  }

  /**
   * {@inheritDoc}
   */
  public function deletePublication(string $external_id): string|null {
    $options = $this->getOptions();
    $content = $this->sendRequest($this->getApiUrl() . '/' . $external_id, $options, 'delete');
    return $content;
  }

  /**
   * Get the options for the request, sets the header and body.
   *
   * @param string|null $body
   *   The body for the request.
   * @param array $headers
   *   The headers for the request.
   *
   * @return array
   *   Return the options used for the request.
   */
  public function getOptions(string $body = NULL, array $headers = []): array {
    $headers['Content-Type'] = 'application/json';

    $api_key = $this->getApiKey();
    if ($api_key) {
      $headers['Authorization'] = $api_key;
    }

    if ($body) {
      $options['body'] = $body;
    }

    $options['headers'] = $headers;
    $options['http_errors'] = FALSE;

    return $options;
  }

  /**
   * Build the request body for the API request.
   *
   * @param \Drupal\Core\Entity\EntityInterface $openwoo_publication
   *   The OpenWooPublication entity.
   *
   * @return string
   *   The encoded request body.
   */
  private function buildRequestBody(EntityInterface $openwoo_publication): string {
    $this->publicationData = unserialize($openwoo_publication->get('data')->value, ['allowed_classes' => FALSE]);
    $portal_url = $openwoo_publication->toLink(NULL, 'canonical', ['absolute' => TRUE])->getUrl()->toString();
    $category_list = $this->publicationFieldsService->getCategoryList();

    $body_array = [
      'publicatiedatum' => $this->publicationData['publication_date'],
      'portalUrl' => $portal_url,
      'gepubliceerd' => $openwoo_publication->isPublished(),
      'categorie' => $category_list[$this->publicationData['category']],
      'titel' => $openwoo_publication->label(),
    ];

    $attachments = $this->getAttachments($openwoo_publication->get('attachments'));
    if (!empty($attachments)) {
      $body_array['bijlagen'] = $attachments;
    }

    if (!empty($this->publicationData['summary'])) {
      $body_array['samenvatting'] = $this->publicationData['summary'];
    }

    if (!empty($this->publicationData['description'])) {
      $body_array['beschrijving'] = $this->publicationData['description'];
    }

    $this->buildOrganization($body_array);
    $this->buildTheme($body_array);
    $this->buildMetadata($body_array);

    return json_encode($body_array);
  }

  /**
   * Get the OpenWooPublication attachments.
   *
   * @param \Drupal\Core\Field\EntityReferenceFieldItemList $attachments
   *   The OpenWooPublication attachments.
   *
   * @return array
   *   The attachments array.
   */
  private function getAttachments(EntityReferenceFieldItemList $attachments): array {
    $attachments_array = [];
    foreach ($attachments as $attachment) {
      $target_id = $attachment->get('target_id')->getValue();
      $media_entity = $this->entityTypeManager->getStorage('media')->load($target_id);
      assert($media_entity instanceof Media);
      $file = $media_entity->field_openwoo_media_file->entity;

      $attachments_array[] = [
        'type' => $file->getMimeType(),
        'status' => $media_entity->status->value,
        'titel' => $media_entity->getName(),
        'label' => $media_entity->label(),
        'url' => $this->fileUrlGenerator->generateAbsoluteString($file->getFileUri()),
      ];
    }
    return $attachments_array;
  }

  /**
   * Build the organization of the request body.
   *
   * @param array $body_array
   *   The body array.
   */
  private function buildOrganization(array &$body_array): void {
    $body_array['organisatie']['naam'] = $this->publicationData['organization_name'];

    if (!empty($this->publicationData['organization_oin'])) {
      $body_array['organisatie']['oin'] = $this->publicationData['organization_oin'];
    }

    if (!empty($this->publicationData['organization_tooi'])) {
      $body_array['organisatie']['tooi'] = $this->publicationData['organization_tooi'];
    }

    if (!empty($this->publicationData['organization_name'])) {
      $body_array['organisatie']['naam'] = $this->publicationData['organization_name'];
    }

    if (!empty($this->publicationData['organization_rsin'])) {
      $body_array['organisatie']['rsin'] = $this->publicationData['organization_rsin'];
    }
  }

  /**
   * Build the theme of the request body.
   *
   * @param array $body_array
   *   The body array.
   */
  private function buildTheme(array &$body_array): void {
    $body_array['themas'][0]['hoofdthema'] = $this->publicationData['main_theme'];

    if (!empty($this->publicationData['sub_theme'])) {
      $body_array['themas'][0]['subthema'] = $this->publicationData['sub_theme'];
    }

    if (!empty($this->publicationData['additional_theme'])) {
      $body_array['themas'][0]['aanvullendThema'] = $this->publicationData['additional_theme'];
    }
  }

  /**
   * Build the metadata of the request body.
   *
   * @param array $body_array
   *   The body array.
   */
  private function buildMetadata(array &$body_array): void {
    $decree = $this->checkIfRequestTypeIsFilled('decree');
    if ($decree) {
      $body_array['metadata']['verzoek']['besluit']['titel'] = $this->publicationData['decree_title'];

      $this->fillRequestFields('decree', 'besluit', $body_array);
    }

    $information_request = $this->checkIfRequestTypeIsFilled('information_request');
    if ($information_request) {
      $body_array['metadata']['verzoek']['informatieverzoek']['titel'] = $this->publicationData['information_request_title'];

      $this->fillRequestFields('information_request', 'informatieverzoek', $body_array);
    }

    $inventory_list = $this->checkIfRequestTypeIsFilled('inventory_list');
    if ($inventory_list) {
      $body_array['metadata']['verzoek']['inventarisatielijst']['titel'] = $this->publicationData['inventory_list_title'];

      $this->fillRequestFields('inventory_list', 'inventarisatielijst', $body_array);
    }

    if (!empty($this->publicationData['request_status'])) {
      $body_array['metadata']['verzoek']['status'] = $this->publicationData['request_status'];
    }

    if (!empty($this->publicationData['request_recipient'])) {
      $body_array['metadata']['verzoek']['ontvangerInformatieverzoek'] = $this->publicationData['request_recipient'];
    }

    if (!empty($this->publicationData['request_receipt_date'])) {
      $body_array['metadata']['verzoek']['ontvangstdatum'] = $this->publicationData['request_receipt_date'];
    }

    if (!empty($this->publicationData['request_term_exceeded'])) {
      $body_array['metadata']['verzoek']['termijnoverschrijding'] = $this->publicationData['request_term_exceeded'];
    }

    if (!empty($this->publicationData['metadata_serialnumber'])) {
      $body_array['metadata']['volgnummer'] = $this->publicationData['metadata_serialnumber'];
    }

    if (!empty($this->publicationData['metadata_decision_date'])) {
      $body_array['metadata']['besluitdatum'] = $this->publicationData['metadata_decision_date'];
    }
  }

  /**
   * Function checks if the request type is filled.
   *
   * The title field is required. So we check if this field is
   * filled, then the request is filled.
   *
   * @param string $type_to_check
   *   The request type to check.
   *
   * @return bool
   *   TRUE if the request type is filled.
   */
  public function checkIfRequestTypeIsFilled(string $type_to_check): bool {
    return !empty($this->publicationData[$type_to_check . '_title']);
  }

  /**
   * Fill the request field names if they are not empty.
   *
   * @param string $key_to_check
   *   The type to check.
   * @param string $body_array_key
   *   The body array key.
   * @param array $body_array
   *   The body array.
   */
  public function fillRequestFields(string $key_to_check, string $body_array_key, array &$body_array): void {
    $request_keys = [
      $key_to_check . '_title' => 'titel',
      $key_to_check . '_url' => 'url',
      $key_to_check . '_status' => 'status',
      $key_to_check . '_type' => 'type',
      $key_to_check . '_category' => 'categorie',
      $key_to_check . '_extension' => 'extension',
    ];
    foreach ($request_keys as $key => $value) {
      if (!empty($this->publicationData[$key])) {
        $body_array['metadata']['verzoek'][$body_array_key][$value] = $this->publicationData[$key];
      }
    }
  }

  /**
   * Helper function to parse the date value.
   *
   * @param string $date
   *   The value to parse as iso date.
   *
   * @return string
   *   The iso date, defaults to an empty string if no valid date is found.
   */
  protected function getIsoDate(string $date): string {
    if (empty($date)) {
      return '';
    }

    return $this->dateFormatter->format($date, 'custom', "Y-m-d\\TH:i:sO", NULL, 'nl');
  }

  /**
   * {@inheritDoc}
   */
  public function getElements(): array {
    $form['api_url'] = [
      '#type' => 'select',
      '#title' => $this->t('Api url'),
      '#empty_option' => $this->t('- Select -'),
      '#options' => $this->getApiUrlOptions(),
      '#description' => $this->t('Choose a connection plugin for publishing content.'),
      '#default_value' => $this->config()->get('api_url'),
    ];

    $api_key_description = $this->t('The API key is required. <br> The API key must be saved in settings.php: <br> $config[&apos;openwoo_publish.openwoo_app&apos;][&apos;api_key&apos;] = &apos;API_KEY&apos;;');
    $api_key_value = $this->configFactory->get($this->pluginDefinition['configName'])->get('api_key');
    $api_key_value = $api_key_value ? $this->maskApiKeyValue($api_key_value) : $this->t('Not set');

    $form['api_key'] = [
      '#type' => 'item',
      '#title' => $this->t('Api key'),
      '#plain_text' => $api_key_value,
      '#description' => $api_key_description,
    ];

    return $form;
  }

  /**
   * Get the api url options.
   *
   * @returns array
   *   the options array.
   */
  public function getApiUrlOptions(): array {
    $options = [
      'https://api.gateway.commonground.nu',
      'https://api.accept.common-gateway.commonground.nu',
      'https://api.common-gateway.commonground.nu',
    ];

    return array_combine($options, $options);
  }

  /**
   * {@inheritDoc}
   */
  public function submitElements(array $form, FormStateInterface $form_state) {
    $this->config()->set('api_url', $form_state->getValue('api_url'))->save();
  }

  /**
   * Mask api key value.
   *
   * @param string $api_key
   *   The api key.
   *
   * @return string
   *   The masked Api key.
   */
  protected function maskApiKeyValue(string $api_key): string {
    $mask = str_repeat('*', strlen($api_key) - 4);
    return $mask . substr($api_key, -4);
  }

}
