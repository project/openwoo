<?php

namespace Drupal\openwoo_publish\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\PluginWithFormsTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\openwoo\OpenWooPublicationFields;
use Drupal\openwoo_publish\OpenWooPublishPluginManager;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for OpenWoo Publish plugins.
 */
abstract class OpenWooPublishPluginBase extends PluginBase implements OpenWooPublishPluginInterface {

  use PluginWithFormsTrait;
  use StringTranslationTrait;
  use LoggerChannelTrait;

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * An http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The openwoo publish' plugin manager.
   *
   * @var \Drupal\openwoo_publish\OpenWooPublishPluginManager
   */
  protected $openWooPublishPluginManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * Service with helpers for the form fields.
   *
   * @var \Drupal\openwoo\OpenWooPublicationFields
   */
  protected $publicationFieldsService;

  /**
   * Constructs a Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The http client.
   * @param \Drupal\openwoo_publish\OpenWooPublishPluginManager $openwoo_publish_plugin_manager
   *   The OpenWoo plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager interface.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator.
   * @param \Drupal\openwoo\OpenWooPublicationFields $publication_fields_service
   *   The OpenWooPublicationFields service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactoryInterface $config_factory,
    ClientInterface $http_client,
    OpenWooPublishPluginManager $openwoo_publish_plugin_manager,
    EntityTypeManagerInterface $entity_type_manager,
    FileUrlGeneratorInterface $file_url_generator,
    OpenWooPublicationFields $publication_fields_service,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
    $this->openWooPublishPluginManager = $openwoo_publish_plugin_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->fileUrlGenerator = $file_url_generator;
    $this->publicationFieldsService = $publication_fields_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('http_client'),
      $container->get('plugin.manager.openwoo_publish'),
      $container->get('entity_type.manager'),
      $container->get('file_url_generator'),
      $container->get('openwoo.publication_fields'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function description(): string {
    return (string) $this->pluginDefinition['description'];
  }

  /**
   * Get the configuration object.
   *
   * @return \Drupal\Core\Config\Config
   *   The configuration object.
   */
  protected function config(): Config {
    return $this->configFactory->getEditable($this->pluginDefinition['configName']);
  }

  /**
   * Send the request to the api.
   *
   * @param string $uri
   *   The uri to send the request to.
   * @param array $options
   *   The options for the request, e.g. the request body.
   * @param string $method
   *   The request method, e.g. 'get' or 'post', defaults to 'get'.
   *
   * @return string|null
   *   The response object or null if no response found.
   *
   * @throws \GuzzleHttp\Exception\ClientException
   *   The client exception from the REST call.
   */
  protected function sendRequest($uri, array $options, string $method): string|NULL {
    try {
      $result = $this->httpClient->request($method, $uri, $options);
      $successful_status_codes = [200, 201, 204];

      if (in_array($result->getStatusCode(), $successful_status_codes)) {
        return $result->getBody()->getContents();
      }
      else {
        $this->getLogger('openwoo_publish')
          ->error('Could not connect to OpenWoo client. We received the following status code: @status_code', ['@status_code' => $result->getStatusCode()]);
        return NULL;
      }
    }
    catch (RequestException $exception) {
      $this->getLogger('openwoo_publish')
        ->error('Could not connect to OpenWoo client: @message', ['@message' => $exception->getMessage()]);
      return NULL;
    }
  }

  /**
   * Get the oin.
   *
   * @return string
   *   The oin.
   */
  protected function getOin(): string {
    return $this->configFactory->get('openwoo.settings')->get('oin');
  }

}
