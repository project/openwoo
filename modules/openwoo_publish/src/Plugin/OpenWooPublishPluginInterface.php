<?php

namespace Drupal\openwoo_publish\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Interface for OpenWooPublish plugins.
 */
interface OpenWooPublishPluginInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label(): string;

  /**
   * Returns the translated plugin description.
   *
   * @return string
   *   The translated title.
   */
  public function description(): string;

  /**
   * Add a publication.
   *
   * @param \Drupal\Core\Entity\EntityInterface $openwoo_publication
   *   The OpenWooPublication entity.
   *
   * @return string|null
   *   The response.
   */
  public function addPublication(EntityInterface $openwoo_publication): string|null;

  /**
   * Update a publication.
   *
   * @param \Drupal\Core\Entity\EntityInterface $openwoo_publication
   *   The OpenWooPublication entity.
   * @param string $external_id
   *   The external_id of the OpenWooPublication.
   *
   * @return string|null
   *   The response.
   */
  public function updatePublication(EntityInterface $openwoo_publication, string $external_id): string|null;

  /**
   * Delete a publication.
   *
   * @param string $external_id
   *   The external_id of the OpenWooPublication.
   *
   * @return string|null
   *   The response.
   */
  public function deletePublication(string $external_id): string|null;

  /**
   * Build the form elements for the settings form.
   *
   * @return array
   *   A list of elements to show on the form.
   */
  public function getElements(): array;

  /**
   * Process the submitted form values.
   *
   * This function is called when the parent form is submitted.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function submitElements(array $form, FormStateInterface $form_state);

}
