<?php

namespace Drupal\openwoo_publish\Plugin\QueueWorker;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\openwoo_publish\OpenWooPublishPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the openwoo_publish_job queue worker.
 *
 * @QueueWorker (
 *   id = "openwoo_publish_job",
 *   title = @Translation("OpenWoo Publish job"),
 *   cron = {"time" = 60}
 * )
 */
class OpenWooPublishJob extends QueueWorkerBase implements ContainerFactoryPluginInterface {
  use StringTranslationTrait;
  use LoggerChannelTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The OpenWoo Publish plugin manager.
   *
   * @var \Drupal\openwoo_publish\OpenWooPublishPluginManager
   */
  protected $openWooPublishPluginManager;

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructor for the OpenWooPublishJobCreator.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager interface.
   * @param \Drupal\openwoo_publish\OpenWooPublishPluginManager $openwoo_publish_plugin_manager
   *   The OpenWoo plugin manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    OpenWooPublishPluginManager $openwoo_publish_plugin_manager,
    ConfigFactoryInterface $config_factory,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->openWooPublishPluginManager = $openwoo_publish_plugin_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.openwoo_publish'),
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $processed = $this->sendItem($data);
    if (!$processed) {
      $publication_id = $data['publication_id'];
      throw new \Exception('OpenWooPublication #' . $publication_id . ' was not processed correctly.');
    }
  }

  /**
   * Process the OpenWoo Publication item in the queue.
   *
   * @param array $data
   *   The item data.
   *
   * @return bool
   *   Returns true if OpenWoo Publication is processed.
   */
  public function sendItem(array $data): bool {
    $active_plugin = $this->openWooPublishPluginManager->getActivePlugin();
    if (!$active_plugin) {
      return FALSE;
    }
    $publication_id = $data['publication_id'];
    $publication_entity = $this->entityTypeManager->getStorage('openwoo_publication')->load($publication_id);

    if (!$publication_entity && $data['action'] !== 'delete') {
      $this->logResponse('OpenWooPublication #' . $publication_id . ' is deleted so it can not be added or updated to', $active_plugin->label(), '');
      return TRUE;
    }

    if ($data['external_id'] === NULL && ($data['action'] === 'delete' || $data['action'] === 'update')) {
      $this->logResponse('OpenWooPublication #' . $publication_id . ' has not been send yet, so it can not be updated or deleted from', $active_plugin->label(), '');
      return TRUE;
    }

    $response = NULL;
    switch ($data['action']) {
      case 'add':
        $response = $active_plugin->addPublication($publication_entity);
        $this->logResponse('OpenWooPublication #' . $publication_id . ' is send to', $active_plugin->label(), $response);
        break;

      case 'update':
        $response = $active_plugin->updatePublication($publication_entity, $data['external_id']);
        $this->logResponse('OpenWooPublication #' . $publication_id . ' is updated in', $active_plugin->label(), $response);
        break;

      case 'delete':
        $response = $active_plugin->deletePublication($data['external_id']);
        $this->logResponse('OpenWooPublication #' . $publication_id . ' is deleted in', $active_plugin->label(), $response);
        break;
    }

    if ($response === NULL) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Log the response.
   *
   * @param string $message
   *   The message.
   * @param string $active_plugin
   *   The active plugin label.
   * @param string|null $response
   *   The response.
   */
  public function logResponse(string $message, string $active_plugin, string|null $response): void {
    if ($response === NULL) {
      return;
    }
    if (!$this->extendedLoggingEnabled()) {
      $this->getLogger('openwoo_publish')
        ->info('@message @active_plugin.', [
          '@message' => $message,
          '@active_plugin' => $active_plugin,
        ]);
      return;
    }

    $this->getLogger('openwoo_publish')
      ->info('@message @active_plugin. Response: @response', [
        '@message' => $message,
        '@active_plugin' => $active_plugin,
        '@response' => $response,
      ]);
  }

  /**
   * Is extended logging enabled.
   *
   * @return bool
   *   Returns the extended logging value.
   */
  public function extendedLoggingEnabled(): bool {
    return $this->configFactory->get('openwoo_publish.settings')->get('extended_logging');
  }

}
