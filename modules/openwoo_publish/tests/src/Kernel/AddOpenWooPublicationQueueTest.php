<?php

namespace Drupal\openwoo_publish\Tests\Kernel;

use Drupal\Core\Database\Database;
use Drupal\Core\Queue\DatabaseQueue;
use Drupal\file\Entity\File;
use Drupal\KernelTests\KernelTestBase;
use Drupal\media\Entity\Media;
use Drupal\openwoo_publish\Entity\OpenWooPublication;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;
use Drupal\Tests\TestFileCreationTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Test adding OpenWoo publications to queue.
 */
class AddOpenWooPublicationQueueTest extends KernelTestBase {
  use UserCreationTrait;
  use MediaTypeCreationTrait;
  use TestFileCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'user',
    'system',
    'filter',
    'field',
    'text',
    'media',
    'image',
    'openwoo',
    'openwoo_publish',
    'options',
    'file',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp():void {
    parent::setUp();
    $this->installEntitySchema('node');
    $this->installEntitySchema('openwoo_publication');
    $this->installEntitySchema('file');
    $this->installEntitySchema('media');
    $this->installEntitySchema('user');
    $this->installSchema('file', ['file_usage']);
    $this->installConfig(['system', 'media', 'openwoo']);

    $config_factory = \Drupal::configFactory();
    $openwoo_Publish_config = $config_factory->getEditable('openwoo_publish.settings');
    $openwoo_Publish_config->set('extended_logging', TRUE);
    $openwoo_Publish_config->save(TRUE);
  }

  /**
   * Tests creating a publication.
   */
  public function testAddPublicationToQueue() {
    $this->createMediaType('file', ['id' => 'file', 'label' => 'File']);
    $file = File::create([
      'filename' => 'Test File',
      'uri' => 'public://my-dir/' . basename('Test File'),
      'filemime' => 'application/pdf',
      'filesize' => 123456,
    ]);
    $file->save();

    $media = Media::create([
      'bundle' => 'file',
      'name' => 'Test file',
      'field_openwoo_media_file' => [
        [
          'target_id' => $file->id(),
        ],
      ],
    ]);
    $media->save();

    $openwoo_publication = OpenWooPublication::create([
      'id' => '7',
      'label' => '7',
      'status' => '1',
      'created' => '1713419589',
      'changed' => '1713419589',
      'external_id' => NULL,
      // cspell:disable-next-line
      'data' => 'a:35:{s:16:"publication_date";s:10:"2024-04-09";s:8:"category";s:6:"3_3_2i";s:7:"summary";s:8:"dsadfsdf";s:11:"description";s:8:"asdfsadf";s:10:"main_theme";s:16:"addfdfafdadaffda";s:9:"sub_theme";s:9:"sdghdfhgf";s:16:"additional_theme";s:7:"dfhfdhd";s:21:"metadata_serialnumber";s:8:"fdhdhfgd";s:22:"metadata_decision_date";s:0:"";s:14:"request_status";s:5:"dhfdh";s:17:"request_recipient";s:5:"dfhfh";s:20:"request_receipt_date";s:0:"";s:21:"request_term_exceeded";s:6:"dfghgf";s:11:"decree_type";s:4:"dfhh";s:12:"decree_title";s:6:"dfghdh";s:12:"decree_label";s:6:"dghdhg";s:10:"decree_url";s:4:"dfhh";s:13:"decree_status";s:4:"dfhd";s:19:"decree_changed_time";s:10:"2024-04-17";s:24:"information_request_type";s:6:"dfhgdh";s:25:"information_request_title";s:6:"dfhdfh";s:25:"information_request_label";s:6:"dfhdfh";s:23:"information_request_url";s:5:"dhfdh";s:26:"information_request_status";s:7:"dfdhdfd";s:32:"information_request_changed_time";s:10:"2024-04-17";s:19:"inventory_list_type";s:9:"dfghdhfgh";s:20:"inventory_list_title";s:6:"dfhfdh";s:20:"inventory_list_label";s:6:"dfhdfh";s:18:"inventory_list_url";s:5:"dfhfd";s:21:"inventory_list_status";s:5:"dfhhd";s:27:"inventory_list_changed_time";s:10:"2024-04-17";s:17:"organization_name";s:18:"Gemeente Eemsdelta";s:16:"organization_oin";s:20:"00000001826228379000";s:17:"organization_tooi";s:8:"testtooi";s:17:"organization_rsin";s:9:"fdhfghgfh";}',
      'attachments' => [
        0 => [
          'target_id' => $media->id(),
        ],
      ],
    ]);

    $openwoo_publication_two = OpenWooPublication::create([
      'id' => '7',
      'label' => '7',
      'status' => '1',
      'created' => '1713419589',
      'changed' => '1713419589',
      'external_id' => '123456789',
      // cspell:disable-next-line
      'data' => 'a:35:{s:16:"publication_date";s:10:"2024-04-09";s:8:"category";s:6:"3_3_2i";s:7:"summary";s:8:"dsadfsdf";s:11:"description";s:8:"asdfsadf";s:10:"main_theme";s:16:"addfdfafdadaffda";s:9:"sub_theme";s:9:"sdghdfhgf";s:16:"additional_theme";s:7:"dfhfdhd";s:21:"metadata_serialnumber";s:8:"fdhdhfgd";s:22:"metadata_decision_date";s:0:"";s:14:"request_status";s:5:"dhfdh";s:17:"request_recipient";s:5:"dfhfh";s:20:"request_receipt_date";s:0:"";s:21:"request_term_exceeded";s:6:"dfghgf";s:11:"decree_type";s:4:"dfhh";s:12:"decree_title";s:6:"dfghdh";s:12:"decree_label";s:6:"dghdhg";s:10:"decree_url";s:4:"dfhh";s:13:"decree_status";s:4:"dfhd";s:19:"decree_changed_time";s:10:"2024-04-17";s:24:"information_request_type";s:6:"dfhgdh";s:25:"information_request_title";s:6:"dfhdfh";s:25:"information_request_label";s:6:"dfhdfh";s:23:"information_request_url";s:5:"dhfdh";s:26:"information_request_status";s:7:"dfdhdfd";s:32:"information_request_changed_time";s:10:"2024-04-17";s:19:"inventory_list_type";s:9:"dfghdhfgh";s:20:"inventory_list_title";s:6:"dfhfdh";s:20:"inventory_list_label";s:6:"dfhdfh";s:18:"inventory_list_url";s:5:"dfhfd";s:21:"inventory_list_status";s:5:"dfhhd";s:27:"inventory_list_changed_time";s:10:"2024-04-17";s:17:"organization_name";s:18:"Gemeente Eemsdelta";s:16:"organization_oin";s:20:"00000001826228379000";s:17:"organization_tooi";s:8:"testtooi";s:17:"organization_rsin";s:9:"fdhfghgfh";}',
      'attachments' => [
        0 => [
          'target_id' => $media->id(),
        ],
      ],
    ]);

    $queue = new DatabaseQueue('openwoo_publish_job', Database::getConnection());
    $queue->createQueue();
    \Drupal::service('openwoo_publish.openwoo_publish_job_creator')->createJobs($openwoo_publication, 'add');
    $number_of_items_in_queue = $queue->numberOfItems();
    $this->assertEquals('1', $number_of_items_in_queue);
    $item_data = $queue->claimItem()->data;
    $this->assertEquals('7', $item_data['publication_id']);
    $this->assertEquals('add', $item_data['action']);
    $this->assertNull($item_data['external_id']);
    $queue->deleteQueue();

    \Drupal::service('openwoo_publish.openwoo_publish_job_creator')->createJobs($openwoo_publication_two, 'update');
    $number_of_items_in_queue = $queue->numberOfItems();
    $this->assertEquals('1', $number_of_items_in_queue);
    $item_data = $queue->claimItem()->data;
    $this->assertEquals('7', $item_data['publication_id']);
    $this->assertEquals('update', $item_data['action']);
    $this->assertEquals('123456789', $item_data['external_id']);
    $queue->deleteQueue();

    \Drupal::service('openwoo_publish.openwoo_publish_job_creator')->createJobs($openwoo_publication_two, 'delete');
    $number_of_items_in_queue = $queue->numberOfItems();
    $this->assertEquals('1', $number_of_items_in_queue);
    $item_data = $queue->claimItem()->data;
    $this->assertEquals('7', $item_data['publication_id']);
    $this->assertEquals('delete', $item_data['action']);
    $this->assertEquals('123456789', $item_data['external_id']);
    $queue->deleteQueue();
  }

}
