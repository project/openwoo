<?php

namespace Drupal\openwoo_publish\Tests\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\file\Entity\File;
use Drupal\KernelTests\KernelTestBase;
use Drupal\media\Entity\Media;
use Drupal\openwoo_publish\Entity\OpenWooPublication;
use Drupal\openwoo_publish\Plugin\OpenWooPublish\OpenWooApp;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;
use Drupal\Tests\TestFileCreationTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * Tests OpenWoo publications.
 *
 * @group openwoo
 */
class OpenWooAppClientTest extends KernelTestBase {
  use UserCreationTrait;
  use MediaTypeCreationTrait;
  use TestFileCreationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'user',
    'system',
    'openwoo',
    'openwoo_publish',
    'file',
    'system',
    'options',
    'user',
    'field',
    'media',
    'image',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('openwoo_publication');
    $this->installEntitySchema('file');
    $this->installEntitySchema('media');
    $this->installEntitySchema('user');
    $this->installSchema('file', ['file_usage']);
    $this->installConfig(['system', 'media']);
  }

  /**
   * The Importer we will test.
   *
   * @var \Drupal\openwoo_publish\Plugin\OpenWooPublish\OpenWooApp
   */
  protected OpenWooApp $openWooAppClient;

  /**
   * Tests publication is published.
   */
  public function testPublishPublicationToOpenWooApp(): void {
    $this->initiateOpenWooPublishClient(new Response(200, [], $this->getDummyResponse('OpenWooAppClientResponse.json')));
    $this->createMediaType('file', ['id' => 'file', 'label' => 'File']);

    FieldStorageConfig::create([
      'field_name' => 'field_openwoo_media_file',
      'entity_type' => 'media',
      'type' => 'file',
    ])->save();

    FieldConfig::create([
      'field_name' => 'field_openwoo_media_file',
      'entity_type' => 'media',
      'bundle' => 'file',
      'label' => 'OpenWoo Media File',
    ])->save();

    $file = File::create([
      'filename' => 'Test File',
      'uri' => 'public://my-dir/' . basename('Test File'),
      'filemime' => 'application/pdf',
      'filesize' => 123456,
    ]);
    $file->save();

    $media = Media::create([
      'bundle' => 'file',
      'name' => 'Test file',
      'field_openwoo_media_file' => [
        [
          'target_id' => $file->id(),
        ],
      ],
    ]);
    $media->save();

    $openwoo_publication = OpenWooPublication::create([
      'id' => '7',
      'label' => '7',
      'status' => '1',
      'created' => '1713419589',
      'changed' => '1713419589',
      // cspell:disable-next-line
      'data' => 'a:35:{s:16:"publication_date";s:10:"2024-04-09";s:8:"category";s:6:"3_3_2i";s:7:"summary";s:8:"dsadfsdf";s:11:"description";s:8:"asdfsadf";s:10:"main_theme";s:16:"addfdfafdadaffda";s:9:"sub_theme";s:9:"sdghdfhgf";s:16:"additional_theme";s:7:"dfhfdhd";s:21:"metadata_serialnumber";s:8:"fdhdhfgd";s:22:"metadata_decision_date";s:0:"";s:14:"request_status";s:5:"dhfdh";s:17:"request_recipient";s:5:"dfhfh";s:20:"request_receipt_date";s:0:"";s:21:"request_term_exceeded";s:6:"dfghgf";s:11:"decree_type";s:4:"dfhh";s:12:"decree_title";s:6:"dfghdh";s:12:"decree_label";s:6:"dghdhg";s:10:"decree_url";s:4:"dfhh";s:13:"decree_status";s:4:"dfhd";s:19:"decree_changed_time";s:10:"2024-04-17";s:24:"information_request_type";s:6:"dfhgdh";s:25:"information_request_title";s:6:"dfhdfh";s:25:"information_request_label";s:6:"dfhdfh";s:23:"information_request_url";s:5:"dhfdh";s:26:"information_request_status";s:7:"dfdhdfd";s:32:"information_request_changed_time";s:10:"2024-04-17";s:19:"inventory_list_type";s:9:"dfghdhfgh";s:20:"inventory_list_title";s:6:"dfhfdh";s:20:"inventory_list_label";s:6:"dfhdfh";s:18:"inventory_list_url";s:5:"dfhfd";s:21:"inventory_list_status";s:5:"dfhhd";s:27:"inventory_list_changed_time";s:10:"2024-04-17";s:17:"organization_name";s:18:"Gemeente Eemsdelta";s:16:"organization_oin";s:20:"00000001826228379000";s:17:"organization_tooi";s:8:"testtooi";s:17:"organization_rsin";s:9:"fdhfghgfh";}',
      'attachments' => [
        0 => [
          'target_id' => $media->id(),
        ],
      ],
    ]);

    $response = $this->openWooAppClient->addPublication($openwoo_publication);
    $decoded_response = json_decode($response);
    $this->assertEquals($decoded_response->_id, $openwoo_publication->getExternalId());
  }

  /**
   * Returns a valid response object.
   *
   * @param string $filename
   *   The filename of the stub to load.
   * @param int $status
   *   The HTTP status code (optional).
   *
   * @return string
   *   The response object.
   */
  private function getDummyResponse(string $filename, int $status = 200): string {
    $module_path = \Drupal::service('module_handler')
      ->getModule('openwoo_publish')
      ->getPath();
    return file_get_contents($module_path . '/tests/src/Stubs/' . $filename);
  }

  /**
   * Initiates the openWooPublishClient with the expected server response.
   *
   * @param \GuzzleHttp\Psr7\Response $expected_response
   *   The expected response.
   */
  private function initiateOpenWooPublishClient(Response $expected_response): void {
    $configuration = [];
    $plugin_id = 'openwoo_app';
    $plugin_definition = [
      'id' => 'openwoo_app',
      'label' => 'OpenWoo.app',
      'description' => 'Publish openwoo publications in OpenWoo.app',
      'configName' => 'openwoo_publish.openwoo_app',
      'class' => 'Drupal\openwoo_publish\Plugin\OpenWooPublish\OpenWooApp',
      'provider' => 'openwoo_publish',
    ];

    $config_factory = \Drupal::configFactory();
    $openwoo_Publish_config = $config_factory->getEditable('openwoo_publish.settings');
    $openwoo_Publish_config->set('client', 'openwoo_app');
    $openwoo_Publish_config->set('extended_logging', TRUE);
    $openwoo_Publish_config->save(TRUE);

    $mock = new MockHandler([$expected_response]);
    $handlerStack = HandlerStack::create($mock);
    $http_client = new Client(['handler' => $handlerStack]);
    $date_formatter = $this->container->get('date.formatter');
    $openwoo_publish_plugin_manager = $this->container->get('plugin.manager.openwoo_publish');
    $entity_type_manager = $this->container->get('entity_type.manager');
    $file_url_generator = $this->container->get('file_url_generator');
    $publication_fields_service = $this->container->get('openwoo.publication_fields');

    $this->openWooAppClient = new OpenWooApp(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $config_factory,
      $http_client,
      $openwoo_publish_plugin_manager,
      $date_formatter,
      $entity_type_manager,
      $file_url_generator,
      $publication_fields_service
    );
  }

}
