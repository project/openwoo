<?php

namespace Drupal\openwoo_publish\Tests\Kernel;

use Drupal\Core\Url;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Test for creating OpenWoo publications.
 */
class OpenWooPublicationAccessTest extends KernelTestBase {
  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'user',
    'system',
    'filter',
    'field',
    'text',
    'media',
    'image',
    'openwoo',
    'openwoo_publish',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp():void {
    parent::setUp();
    $this->installSchema('node', 'node_access');
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('media');
    $this->installEntitySchema('openwoo_publication');
    $this->installConfig('filter');
    $this->installConfig('node');
    $this->installConfig(['openwoo']);
  }

  /**
   * Tests creating a publication.
   */
  public function testCreatePublicationAccess() {
    // User with all permissions.
    $user = $this->setUpCurrentUser([], [
      'view openwoo publications',
      'add openwoo publications',
      'edit openwoo publications',
      'delete openwoo publications',
      'administer openwoo publications',
    ]);
    $user->setUsername('authenticated');
    // User with no permissions.
    $user_false = $this->setUpCurrentUser([], []);
    $user_false->setUsername('anonymous');

    // User can access publication overview page.
    $this->checkAccessForRouteAndUser('entity.openwoo_publication.collection', $user, TRUE);
    // No-right user can NOT access publication overview page.
    $this->checkAccessForRouteAndUser('entity.openwoo_publication.collection', $user_false, FALSE);
    // User can access publication add page.
    $this->checkAccessForRouteAndUser('entity.openwoo_publication.add_form', $user, TRUE);
    // No-right user can NOT access publication add page.
    $this->checkAccessForRouteAndUser('entity.openwoo_publication.add_form', $user_false, FALSE);
  }

  /**
   * Checks if a user has access to a specific route.
   *
   * @param string $route_name
   *   The operation to check access for.
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The user.
   * @param bool $expected_result
   *   Expected result for the check.
   */
  public function checkAccessForRouteAndUser($route_name, $user, $expected_result) {
    $url = Url::fromRoute($route_name);
    $access = $url->access($user);
    $this->assertEquals($expected_result, $access, $this->nodeAccessAssertMessage($route_name, $access, $expected_result, $user->getAccountName()));
  }

  /**
   * Constructs an assert message to display which node access was tested.
   *
   * @param string $route_name
   *   The operation to check access for.
   * @param bool $result
   *   Actual result for the check.
   * @param bool $expected_result
   *   Expected result for the check.
   * @param string $user
   *   Name of the user.
   *
   * @return string
   *   An assert message string for the test that was performed.
   */
  public function nodeAccessAssertMessage($route_name, $result, $expected_result, $user) {
    return sprintf('Node access for %s is %s and not %s for user %s.',
      $route_name,
      $result ? 'true' : 'false',
      $expected_result ? 'true' : 'false',
      $user
    );
  }

}
