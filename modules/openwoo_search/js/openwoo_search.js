(function (Drupal) {
  Drupal.behaviors.openwoo_search = {
    attach(context, settings) {
      // elements.forEach(function () {
      document
        .querySelector('#openwoo-submit')
        .addEventListener('click', function (e) {
          e.preventDefault();
          const searchTextValue = document.querySelector(
            'input[name="search_text"]',
          ).value;
          const yearValue = document.querySelector('select[name="year"]').value;
          const categoryValue = document.querySelector(
            'select[name="category_id"]',
          ).value;
          const url = document.querySelector(
            'input[name="ajax_filter_url"]',
          ).value;

          Drupal.ajax({
            submit: {
              search_text: searchTextValue,
              category_id: categoryValue,
              year: yearValue,
            },
            url,
          }).execute();
        });
    },
  };
})(Drupal);
