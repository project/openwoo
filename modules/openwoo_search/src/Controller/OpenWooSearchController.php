<?php

namespace Drupal\openwoo_search\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\openwoo\OpenWooPublicationFields;
use Drupal\openwoo_search\OpenWooSearchPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for the OpenWoo Search pages.
 */
class OpenWooSearchController extends ControllerBase {

  /**
   * The plugin manager for 'OpenWoo search'.
   *
   * @var \Drupal\openwoo_search\OpenWooSearchPluginManagerInterface
   */
  protected $openWooSearchPluginManager;

  /**
   * Service with helpers for the form fields.
   *
   * @var \Drupal\openwoo\OpenWooPublicationFields
   */
  protected $publicationFieldsService;

  /**
   * Constructs a OpenWooSearchController object.
   *
   * @param \Drupal\openwoo_search\OpenWooSearchPluginManagerInterface $openwoo_search_plugin_manager
   *   The OpenWoo Search manager.
   * @param \Drupal\openwoo\OpenWooPublicationFields $publication_fields_service
   *   The OpenWooPublicationFields service.
   */
  public function __construct(OpenWooSearchPluginManagerInterface $openwoo_search_plugin_manager, OpenWooPublicationFields $publication_fields_service) {
    $this->openWooSearchPluginManager = $openwoo_search_plugin_manager;
    $this->publicationFieldsService = $publication_fields_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.openwoo_search'),
      $container->get('openwoo.publication_fields')
    );
  }

  /**
   * Create the OpenWoo Search detail page.
   *
   * @return array
   *   The render array for the OpenWoo Search detail page.
   */
  public function buildDetailPage(string $external_publication_id = NULL) {
    $active_plugin = $this->openWooSearchPluginManager->getActivePlugin();
    $details = $active_plugin->getDetails($external_publication_id);
    $build = [];
    if (empty($details)) {
      $build['info_block'] = [
        '#theme' => 'openwoo_search_detail_info',
        '#description' => $this->t('Something went wrong while fetching the dossier. Please try another dossier.'),
      ];

      return $build;
    }

    $publication_data = $this->publicationFieldsService->getPublication((array) $details, 'view');
    $build['details'] = [
      '#theme' => 'openwoo_publish_publication',
      '#publication_data' => $publication_data,
    ];

    $attachments = $this->parseAttachments($details['attachments']);
    if (!empty($attachments)) {
      $generated_attachments = $this->buildAttachmentsContainer($attachments);
      $build['attachments'] = [
        '#theme' => 'openwoo_publish_publication',
        '#publication_data' => [
          'attachments' => $generated_attachments,
        ],
      ];
    }

    return $build;
  }

  /**
   * Parse the attachments.
   *
   * @param array $documents
   *   The documents.
   *
   * @return array
   *   The attachments.
   */
  protected function parseAttachments(array $documents) {
    $files = [];

    foreach ($documents as $document) {
      if (!$document['status']) {
        continue;
      }

      $link = Link::fromTextAndUrl($document['file_name'], Url::fromUri($document['link']))->toRenderable();
      $link['#attributes'] = [
        'class' => [
          'attachments',
          'openwoo__attachments',
        ],
      ];

      $files[] = $link;
    }

    return $files;
  }

  /**
   * Build attachments container.
   *
   * @param array $attachments
   *   The documents.
   *
   * @return array
   *   The generated attachments.
   */
  protected function buildAttachmentsContainer(array $attachments) {
    $attachment_items = [
      '#type' => 'container',
      '#tag' => 'div',
      '#theme' => 'openwoo_search_attachments',
      '#attachments' => $attachments,
    ];
    return [
      '#theme' => 'openwoo_publish_container',
      '#item_name' => 'attachments',
      '#item_type' => 'fieldset',
      '#item_title' => $this->t('Attachments'),
      '#item_elements' => [
        'attachment_items' => $attachment_items,
      ],
    ];
  }

  /**
   * Get page title based on external publication id.
   *
   * @param string|null $external_publication_id
   *   The external publication id.
   *
   * @return string
   *   The page title.
   */
  public function getPageTitle(string $external_publication_id = NULL): string {
    $active_plugin = $this->openWooSearchPluginManager->getActivePlugin();
    $details = $active_plugin->getDetails($external_publication_id);
    if (empty($details)) {
      return $this->t('Publication not found');
    }
    return $details['title'];
  }

}
