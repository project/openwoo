<?php

namespace Drupal\openwoo_search\Form;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Component\Render\MarkupTrait;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\Core\Pager\PagerParametersInterface;
use Drupal\Core\Url;
use Drupal\openwoo_search\OpenWooSearchPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Ajax form load OpenWoo documents.
 */
class OpenWooAjaxForm extends FormBase implements ContainerInjectionInterface {

  use MarkupTrait;

  /**
   * The plugin manager for 'OpenWoo search'.
   *
   * @var \Drupal\openwoo_search\OpenWooSearchPluginManagerInterface
   */
  protected $openWooSearchPluginManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The active search plugin.
   *
   * @var \Drupal\openwoo_search\Plugin\OpenWooSearchPluginInterface
   */
  protected $activeSearchPlugin;

  const EMPTY_PARAM = '___';

  /**
   * The pager manager interface.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * The pager parameters interface.
   *
   * @var \Drupal\Core\Pager\PagerParametersInterface
   */
  protected $pagerParameters;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs a new OpenWooAjaxForm object.
   *
   * @param \Drupal\openwoo_search\OpenWooSearchPluginManagerInterface $openwoo_search_plugin_manager
   *   The OpenWoo plugin manager.
   * @param \Drupal\Core\Pager\PagerManagerInterface $pager_manager
   *   The pager manager.
   * @param \Drupal\Core\Pager\PagerParametersInterface $pager_parameters
   *   The pager parameter interface.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request_stack.
   */
  public function __construct(OpenWooSearchPluginManagerInterface $openwoo_search_plugin_manager, PagerManagerInterface $pager_manager, PagerParametersInterface $pager_parameters, FormBuilderInterface $form_builder, RequestStack $request_stack) {
    $this->openWooSearchPluginManager = $openwoo_search_plugin_manager;
    $this->pagerManager = $pager_manager;
    $this->pagerParameters = $pager_parameters;
    $this->formBuilder = $form_builder;
    $this->requestStack = $request_stack;
    $this->activeSearchPlugin = $this->openWooSearchPluginManager->getActivePlugin();
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.openwoo_search'),
      $container->get('pager.manager'),
      $container->get('pager.parameters'),
      $container->get('form_builder'),
      $container->get('request_stack'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'openwoo_search_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $default_category_id = ''): array {
    $form['openwoo_input_wrapper'] = $this->getOpenWooInputWrapper();
    $form['openwoo_wrapper'] = $this->getOpenWooWrapper();

    $form['openwoo_input_wrapper']['search_text'] = $this->getSearchField();
    if (empty($default_category_id)) {
      $form['openwoo_input_wrapper']['category_id'] = $this->getCategoryField();
    }
    $form['openwoo_input_wrapper']['year'] = $this->getYearField();
    $form['openwoo_input_wrapper']['submit'] = $this->getSubmitButton();
    $form['openwoo_input_wrapper']['ajax_filter_url'] = $this->getAjaxFilterUrl();

    $search_results = $this->search($default_category_id);
    $form['openwoo_wrapper']['total_items'] = $this->getPageField(!empty($search_results['items']) ? $search_results['items'] : 0);
    $form['openwoo_wrapper']['openwoo_results'] = $this->getOpenWooResults(
      !empty($search_results['results']) ? $search_results['results'] : [],
      !empty($search_results['search_stats']) ? $search_results['search_stats'] : new MarkupInterface()
    );

    $search_text = $form['openwoo_input_wrapper']['search_text']['#value'] ?? '';
    $category_id = $form['openwoo_input_wrapper']['category_id']['#value'] ?? $default_category_id;
    $year = $form['openwoo_input_wrapper']['year']['#value'] ?? '';
    $total_items = $form['openwoo_wrapper']['total_items']['#default_value'];
    $this->addPager($form, $total_items, $search_text, $category_id, $year);

    $form['#prefix'] = '<div id="openwoo-search-form-wrapper">';
    $form['#suffix'] = '</div>';

    return $form;
  }

  /**
   * Add the pager to the form.
   */
  protected function addPager(array &$form, int $total_items, string $search_text = '', string $category_id = '', string $year = '') {
    $this->pagerManager->createPager($total_items, $this->activeSearchPlugin->getItemsPerPage());
    $form['openwoo_wrapper']['pager'] = [
      '#id' => 'openwoo-search-pager',
      '#type' => 'pager',
      '#element' => 0,
      '#route_name' => 'openwoo_search.ajax.pager',
      '#route_parameters' => [
        'search_text' => empty($search_text) ? self::EMPTY_PARAM : $search_text,
        'category_id' => empty($category_id) ? self::EMPTY_PARAM : $category_id,
        'year' => empty($year) ? self::EMPTY_PARAM : $year,
      ],
    ];
  }

  /**
   * Get the data for the search field.
   *
   * @return array
   *   The search field.
   */
  protected function getSearchField(): array {
    return [
      '#type' => 'textfield',
      '#size' => 30,
      '#placeholder' => $this->t('Search ...'),
      '#attributes' => [
        'aria-controls' => 'openwoo-wrapper',
      ],
    ];
  }

  /**
   * Get the data for the category field.
   *
   * @return array
   *   The category field.
   */
  protected function getCategoryField(): array {
    return [
      '#type' => 'select',
      '#options' => $this->activeSearchPlugin->getCategoryOptions(),
      '#empty_value' => NULL,
      '#empty_option' => $this->t('- All categories -'),
      '#attributes' => [
        'aria-controls' => 'openwoo-wrapper',
      ],
    ];
  }

  /**
   * Get the data for the year field.
   *
   * @return array
   *   The year field.
   */
  protected function getYearField(): array {
    return [
      '#type' => 'select',
      '#options' => $this->activeSearchPlugin->getyearOptions(),
      '#empty_value' => NULL,
      '#empty_option' => $this->t('- All years -'),
      '#attributes' => [
        'aria-controls' => 'openwoo-wrapper',
      ],
    ];
  }

  /**
   * The ajax filter url field.
   *
   * @return array
   *   The hidden ajax filter url field.
   */
  protected function getAjaxFilterUrl(): array {
    return [
      '#type' => 'hidden',
      '#value' => Url::fromRoute('openwoo_search.ajax.filter')->toString(),
    ];
  }

  /**
   * Get the data for the submit button.
   *
   * @return array
   *   The submit button.
   */
  protected function getSubmitButton(): array {
    return [
      '#id' => 'openwoo-submit',
      '#type' => 'submit',
      '#value' => $this->t('Search'),
    ];
  }

  /**
   * Get the OpenWoo wrapper.
   *
   * @return array
   *   The OpenWoo wrapper.
   */
  protected function getOpenWooWrapper(): array {
    return [
      '#type' => 'container',
      '#tag' => 'div',
      '#attributes' => [
        'id' => 'openwoo-wrapper',
        'class' => 'openwoo-wrapper',
        'aria-live' => 'assertive',
        'aria-atomic' => 'true',
      ],
    ];
  }

  /**
   * Get the OpenWoo form input wrapper.
   *
   * @return array
   *   The OpenWoo input wrapper.
   */
  protected function getOpenWooInputWrapper(): array {
    return [
      '#type' => 'container',
      '#tag' => 'div',
      '#attributes' => [
        'id' => 'openwoo-input-wrapper',
        'class' => 'openwoo-input-wrapper',
        'aria-live' => 'assertive',
        'aria-atomic' => 'true',
      ],
    ];
  }

  /**
   * Hidden field for the current page.
   *
   * @param int $page
   *   The current page.
   *
   * @return array
   *   The hidden field for the current page.
   */
  protected function getPageField(int $page): array {
    return [
      '#type' => 'hidden',
      '#default_value' => $page,
    ];
  }

  /**
   * Get the OpenWoo results in a container.
   *
   * @param array $results
   *   The search results.
   * @param \Drupal\Component\Render\MarkupInterface $search_stats
   *   The search statistics.
   *
   * @return array
   *   The container for the results with the results.
   */
  protected function getOpenWooResults(array $results, MarkupInterface $search_stats): array {
    return [
      '#type' => 'container',
      '#tag' => 'div',
      '#theme' => 'openwoo_results',
      '#results' => $results,
      '#search_stats' => $search_stats,
      '#attributes' => [
        'id' => 'openwoo-results-wrapper',
        'aria-live' => 'assertive',
        'aria-atomic' => 'true',
      ],
    ];
  }

  /**
   * Reload the form by ajax using the page selected by the pager.
   *
   * @param string $search_text
   *   The search string.
   * @param string $category_id
   *   The category id.
   * @param string $year
   *   The year.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An Ajax response with the updated form.
   */
  public function updatePager(string $search_text = '', string $category_id = '', string $year = ''): AjaxResponse {
    $response = new AjaxResponse();
    $form = $this->formBuilder->getForm(OpenWooAjaxForm::class);

    $search_text = str_replace(self::EMPTY_PARAM, '', $search_text);
    $selected_category = str_replace(self::EMPTY_PARAM, '', $category_id) ?? NULL;
    $selected_year = str_replace(self::EMPTY_PARAM, '', $year) ?? NULL;

    $request = $this->requestStack->getCurrentRequest();
    $page = ($request->get('page') ?? 0) + 1;

    $this->updateForm($form, $search_text, $selected_year, $selected_category, $page);

    $response->addCommand(new ReplaceCommand('#openwoo-search-form-wrapper', $form));
    return $response;
  }

  /**
   * Update the form.
   *
   * @param array $form
   *   The form.
   * @param string $search_text
   *   The search text.
   * @param string $selected_year
   *   The selected year.
   * @param string $selected_category
   *   The selected category.
   * @param int $page
   *   The page.
   */
  protected function updateForm(array &$form, string $search_text = '', string $selected_year = '', string $selected_category = '', int $page = 1) {
    $search_results = $this->activeSearchPlugin->search($search_text, $selected_year, $selected_category, $page);
    $form['openwoo_input_wrapper']['search_text']['#value'] = $search_text;
    $form['openwoo_input_wrapper']['category_id']['#value'] = $selected_category;
    $form['openwoo_input_wrapper']['year']['#value'] = $selected_year;
    $form['openwoo_wrapper']['total_items']['#default_value'] = $search_results['items'];
    $form['openwoo_wrapper']['openwoo_results']['#results'] = $search_results['publications'];
    $form['openwoo_wrapper']['openwoo_results']['#search_stats'] = $search_results['search_stats'];
    $this->addPager($form, $search_results['items'], $search_text, $selected_category, $selected_year);
  }

  /**
   * Update the form based on the form filters.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response with the updated form.
   */
  public function ajaxFilter(): AjaxResponse {
    $response = new AjaxResponse();
    $form = $this->formBuilder->getForm(OpenWooAjaxForm::class);

    $request = $this->requestStack->getCurrentRequest();
    $search_text = $request->get('search_text') ?? '';
    $selected_category = $request->get('category_id') ?? '';
    $selected_year = $request->get('year') ?? '';
    $request->query->set('page', 0);

    $this->updateForm($form, $search_text, $selected_year, $selected_category);

    $response->addCommand(new ReplaceCommand('#openwoo-search-form-wrapper', $form));
    return $response;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Get the search results, pager and facets.
   *
   * @param string $default_category_id
   *   (Optional) The default category ID to filter on.
   *
   * @return array
   *   The search array.
   */
  protected function search(string $default_category_id = ''): array {
    $request = $this->requestStack->getCurrentRequest();
    $search_text = $request->get('search_text') ?? '';
    $category_id = $request->get('category_id') ?? $default_category_id;
    $year = $request->get('year') ?? '';

    $search_text = str_replace(self::EMPTY_PARAM, '', $search_text);
    $selected_category = str_replace(self::EMPTY_PARAM, '', $category_id) ?? NULL;
    $selected_year = str_replace(self::EMPTY_PARAM, '', $year) ?? NULL;
    $page = ($request->get('page') ?? 0) + 1;

    $results = $this->activeSearchPlugin->search($search_text, $selected_year, $selected_category, $page);
    if (NULL === $results) {
      return [];
    }

    return [
      'items' => $results['items'],
      'result_limit' => $results['results_per_page'],
      'page' => $results['current_page'],
      'max_page' => $results['number_of_pages'],
      'results' => $results['publications'],
      'search_stats' => $results['search_stats'],
    ];
  }

}
