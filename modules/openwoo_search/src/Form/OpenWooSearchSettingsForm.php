<?php

namespace Drupal\openwoo_search\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\openwoo_search\OpenWooSearchPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OpenWooSearchSettingsForm. Config form for the OpenWoo Search module.
 *
 * @package Drupal\openwoo\Form
 */
class OpenWooSearchSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'openwoo_search.settings';

  /**
   * The OpenWooSearch' plugin manager.
   *
   * @var \Drupal\openwoo_search\OpenWooSearchPluginManager
   */
  protected $openWooSearchPluginManager;

  /**
   * SettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\openwoo_search\OpenWooSearchPluginManager $openwoo_search_plugin_manager
   *   The OpenWoo plugin manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, OpenWooSearchPluginManager $openwoo_search_plugin_manager) {
    parent::__construct($config_factory);
    $this->openWooSearchPluginManager = $openwoo_search_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.openwoo_search')
    );
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'openwoo_search.settings',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'openwoo_search_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(static::SETTINGS);

    $form['client'] = [
      '#type' => 'select',
      '#title' => $this->t('Client'),
      '#empty_option' => $this->t('- Select -'),
      '#options' => $this->openWooSearchPluginManager->getPlugins(),
      '#description' => $this->t('Choose a connection plugin for searching content.'),
      '#default_value' => $config->get('client'),
      '#ajax' => [
        'callback' => '::updateClientSettings',
        'wrapper' => 'client-settings-wrapper',
      ],
    ];

    $form['items_per_page'] = [
      '#type' => 'select',
      '#title' => $this->t('Items per page'),
      '#description' => $this->t('The amount of results that will be displayed in the search block.'),
      '#options' => [
        10 => '10',
        25 => '25',
        50 => '50',
      ],
      '#default_value' => $config->get('items_per_page') ?? '',
    ];

    $form['client_settings'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'client-settings-wrapper'],
    ];

    // Add the current client settings to the form.
    $client = $form_state->getValue('client') ?? $config->get('client');
    if ($client) {
      $form['client_settings'] += $this->openWooSearchPluginManager->getFormElements($client, $form, $form_state);
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * AJAX callback to update the client settings form.
   *
   * @param array $form
   *   The form render array.
   *
   * @return array
   *   The form elements.
   */
  public function updateClientSettings(array $form): array {
    return $form['client_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('openwoo_search.settings')
      ->set('client', $form_state->getValue('client'))
      ->set('items_per_page', $form_state->getValue('items_per_page'))
      ->save();
    $this->openWooSearchPluginManager->submitFormElements($form_state->getValue('client'), $form['client_settings'], $form_state);
    parent::submitForm($form, $form_state);
  }

}
