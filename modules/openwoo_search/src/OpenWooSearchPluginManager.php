<?php

namespace Drupal\openwoo_search;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * OpenWooSearch plugin manager.
 */
class OpenWooSearchPluginManager extends DefaultPluginManager implements OpenWooSearchPluginManagerInterface {
  use StringTranslationTrait;
  use LoggerChannelTrait;

  /**
   * List of all instances.
   *
   * @var array
   */
  protected $instances = [];

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructs OpenWooSearchPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory) {
    parent::__construct(
      'Plugin/OpenWooSearch',
      $namespaces,
      $module_handler,
      'Drupal\openwoo_search\Plugin\OpenWooSearchPluginInterface',
      'Drupal\openwoo_search\Annotation\OpenWooSearch'
    );
    $this->alterInfo('openwoo_search_info');
    $this->setCacheBackend($cache_backend, 'openwoo_search_plugins');
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugins(): array {
    $plugins = $this->getDefinitions();
    foreach ($plugins as $plugin_id => $plugin) {
      $plugins[$plugin_id] = $plugin['label']->render();
    }
    return $plugins;
  }

  /**
   * {@inheritdoc}
   */
  public function getActivePlugin(): ?object {
    if ($plugin_id = $this->configFactory->get('openwoo_search.settings')->get('client')) {
      return $this->createInstance($plugin_id);
    }

    $this->getLogger('openwoo_search')
      ->error('No publish plugin selected.');
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormElements($plugin_id, array &$form, FormStateInterface $form_state): array {
    $plugin = $this->createInstance($plugin_id);

    return $plugin->getElements();
  }

  /**
   * {@inheritdoc}
   */
  public function submitFormElements(string $plugin_id, array $form, FormStateInterface $form_state): void {
    $plugin = $this->createInstance($plugin_id);
    $plugin->submitElements($form, $form_state);
  }

}
