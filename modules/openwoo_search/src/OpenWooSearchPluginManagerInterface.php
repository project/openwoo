<?php

namespace Drupal\openwoo_search;

use Drupal\Core\Form\FormStateInterface;

/**
 * OpenWoo Search plugin manager.
 */
interface OpenWooSearchPluginManagerInterface {

  /**
   * Get a list of all connection plugins.
   *
   * @return array
   *   A list of plugin names, keyed by plugin id.
   */
  public function getPlugins(): array;

  /**
   * Get the active plugin instance.
   *
   * @return object|null
   *   The active plugin or null if none set.
   */
  public function getActivePlugin(): ?object;

  /**
   * Get the form elements for a specific plugin.
   *
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state interface.
   *
   * @return array
   *   The form elements.
   */
  public function getFormElements($plugin_id, array &$form, FormStateInterface $form_state): array;

  /**
   * Submit all form elements from the plugin.
   *
   * @param string $plugin_id
   *   The current form.
   * @param array $form
   *   The current form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function submitFormElements(string $plugin_id, array $form, FormStateInterface $form_state): void;

}
