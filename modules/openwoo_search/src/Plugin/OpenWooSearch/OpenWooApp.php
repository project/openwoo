<?php

namespace Drupal\openwoo_search\Plugin\OpenWooSearch;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\openwoo\OpenWooPublicationFields;
use Drupal\openwoo_search\OpenWooSearchPluginManager;
use Drupal\openwoo_search\Plugin\OpenWooSearchPluginBase;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the openwoo_app.
 *
 * @OpenWooSearch(
 *   id = "openwoo_app",
 *   label = @Translation("OpenWoo.app"),
 *   description = @Translation("Search OpenWoo publications in OpenWoo.app"),
 *   configName = "openwoo_search.openwoo_app"
 * )
 */
class OpenWooApp extends OpenWooSearchPluginBase {

  use StringTranslationTrait;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Set the cache to expire after 4 hours.
   */
  const CACHE_EXPIRE = 14400;

  /**
   * Constructs a Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The http client.
   * @param \Drupal\openwoo_search\OpenWooSearchPluginManager $openwoo_search_plugin_manager
   *   The OpenWoo plugin manager.
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\openwoo\OpenWooPublicationFields $publication_fields_service
   *   The OpenWooPublicationFields service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactoryInterface $config_factory,
    ClientInterface $http_client,
    OpenWooSearchPluginManager $openwoo_search_plugin_manager,
    DateFormatter $date_formatter,
    CacheBackendInterface $cache,
    TimeInterface $time,
    OpenWooPublicationFields $publication_fields_service,
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $config_factory,
      $http_client,
      $openwoo_search_plugin_manager,
      $publication_fields_service);
    $this->dateFormatter = $date_formatter;
    $this->cache = $cache;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('http_client'),
      $container->get('plugin.manager.openwoo_search'),
      $container->get('date.formatter'),
      $container->get('cache.default'),
      $container->get('datetime.time'),
      $container->get('openwoo.publication_fields')
    );
  }

  /**
   * Get the api url.
   *
   * @returns string
   *    The api url.
   */
  public function getApiUrl(): string {
    return $this->config()->get('api_url') . '/api/publicaties';
  }

  /**
   * Get the api key.
   *
   * @return string|null
   *   The api key.
   */
  public function getApiKey(): string|NULL {
    return $this->configFactory->get($this->pluginDefinition['configName'])->get('api_key');
  }

  /**
   * {@inheritDoc}
   */
  public function search(string $search_text = NULL, string $year = NULL, string $category = NULL, int $page = NULL): array|null {
    $query = [
      'organisatie.oin' => $this->getOin(),
      '_search' => $search_text ?? NULL,
      'publicatiedatum[after]' => $year ? $year . '-01-01T00:00:00Z' : NULL,
      'publicatiedatum[before]' => $year ? $year . '-12-31T23:59:59Z' : NULL,
      'categorie' => $category ?? NULL,
      'limit' => $this->getItemsPerPage(),
      '_page' => $page ?? 1,
    ];

    // Remove empty values from the $params array.
    $query = array_filter($query, fn($value) => !empty($value));

    $options = $this->getOptions($query);
    $content = $this->sendRequest($this->getApiUrl(), $options);

    if (empty($content)) {
      return NULL;
    }

    return $this->parseResults(json_decode($content), $search_text);
  }

  /**
   * {@inheritDoc}
   */
  public function getDetails(string $external_publication_id): array|null {
    $options = $this->getOptions();
    $content = $this->sendRequest($this->getApiUrl() . '/' . $external_publication_id, $options);
    return (!empty($content)) ? $this->getPublicationValues(json_decode($content)) : NULL;
  }

  /**
   * Get publication values.
   *
   * @param object $publication
   *   The query parameters.
   *
   * @return array
   *   The publication values.
   */
  public function getPublicationValues(object $publication): array {
    $category_list = $this->publicationFieldsService->getCategoryList();
    array_shift($category_list);
    $category_list = array_flip($category_list);
    $category = array_key_exists($publication->categorie, $category_list) ? $category_list[$publication->categorie] : '';
    $attachments = $this->getAttachments($publication->bijlagen);

    return [
      'title' => $publication->titel,
      'publication_date' => $publication->publicatiedatum,
      'category' => $category,
      'summary' => $publication->samenvatting ?? '',
      'description' => $publication->beschrijving ?? '',
      'main_theme' => $publication->themas[0]->hoofdthema,
      'sub_theme' => $publication->themas[0]->subthema ?? '',
      'additional_theme' => $publication->themas[0]->aanvullendThema ?? '',
      'metadata_serialnumber' => $publication->metadata->volgnummer ?? '',
      'metadata_decision_date' => $publication->metadata->besluitdatum ?? '',
      'request_status' => $publication->metadata->verzoek->status ?? '',
      'request_recipient' => $publication->metadata->verzoek->ontvangerInformatieverzoek ?? '',
      'request_receipt_date' => $publication->metadata->verzoek->ontvangstdatum ?? '',
      'request_term_exceeded' => $publication->metadata->verzoek->termijnoverschrijding ?? '',
      'decree_title' => $publication->metadata->verzoek->besluit->titel ?? '',
      'decree_url' => $publication->metadata->verzoek->besluit->url ?? '',
      'decree_status' => $publication->metadata->verzoek->besluit->status ?? '',
      'decree_type' => $publication->metadata->verzoek->besluit->type ?? '',
      'decree_category' => $publication->metadata->verzoek->besluit->categorie ?? '',
      'decree_extension' => $publication->metadata->verzoek->besluit->extension ?? '',
      'information_request_title' => $publication->metadata->verzoek->informatieverzoek->titel ?? '',
      'information_request_url' => $publication->metadata->verzoek->informatieverzoek->url ?? '',
      'information_request_status' => $publication->metadata->verzoek->informatieverzoek->status ?? '',
      'information_request_type' => $publication->metadata->verzoek->informatieverzoek->type ?? '',
      'information_request_category' => $publication->metadata->verzoek->informatieverzoek->categorie ?? '',
      'information_request_extension' => $publication->metadata->verzoek->informatieverzoek->extension ?? '',
      'inventory_list_title' => $publication->metadata->verzoek->inventarisatielijst->titel ?? '',
      'inventory_list_url' => $publication->metadata->verzoek->inventarisatielijst->url ?? '',
      'inventory_list_status' => $publication->metadata->verzoek->inventarisatielijst->status ?? '',
      'inventory_list_type' => $publication->metadata->verzoek->inventarisatielijst->type ?? '',
      'inventory_list_category' => $publication->metadata->verzoek->inventarisatielijst->categorie ?? '',
      'inventory_list_extension' => $publication->metadata->verzoek->inventarisatielijst->extension ?? '',
      'organization_name' => $publication->organisatie->naam ?? '',
      'organization_oin' => $publication->organisatie->oin ?? '',
      'organization_tooi' => $publication->organisatie->tooi ?? '',
      'organization_rsin' => $publication->organisatie->rsin ?? '',
      'attachments' => $attachments,
    ];
  }

  /**
   * Get the options for the request, sets the header, query and api_key.
   *
   * @param array $query_params
   *   The query parameters.
   * @param array $headers
   *   The headers for the request.
   *
   * @return array
   *   Return the options used for the request.
   */
  public function getOptions(array $query_params = [], array $headers = []): array {
    $api_key = $this->getApiKey();
    if ($api_key) {
      $headers['Authorization'] = $api_key;
    }
    return [
      'query' => $query_params,
      'headers' => $headers,
      'http_errors' => FALSE,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getSearchStats(object $json, string $search_text = NULL): TranslatableMarkup {
    $page = (int) $json->page;
    $results_per_page = (int) $json->limit;
    $first = $json->total > 0 ? ($results_per_page * ($page - 1) + 1) : 0;
    $last = ($results_per_page * ($page - 1) + $results_per_page);
    $results = $first . ' - ' . min($last, $json->total);

    if (!isset($search_text)) {
      return $this->t("Result @results of @result_total results", [
        '@results' => $results,
        '@result_total' => $json->total,
      ]);
    }

    return $this->t("Result @results of @result_total results for search term: @search_term", [
      '@results' => $results,
      '@result_total' => $json->total,
      '@search_term' => $search_text,
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function parseResults(object $json, string $search_text = NULL): array {
    $publications = [];

    foreach ($json->results as $item) {
      $result = new OpenWooSearchResult();
      $result->setId($item->_id);
      $result->setTitle($item->titel);
      $description = $item->beschrijving ?? NULL;
      if ($description !== NULL) {
        $description = html_entity_decode($description);
        $result->setDescription(strip_tags($description));
      }
      $summary = $item->samenvatting ?? NULL;
      if ($summary !== NULL) {
        $summary = html_entity_decode($summary);
        $result->setSummary(strip_tags($summary));
      }

      $result->setCategory($item->categorie);
      $result->setPublishDate($item->publicatiedatum);
      $result->setPublishDateIso($this->getIsoDate($item->publicatiedatum));
      $result->setRemoteLink('/openwoo-search/result/' . $item->_id);

      $publications[] = $result;
    }

    return [
      'items' => $json->total,
      'pages' => $json->pages,
      'results_per_page' => $json->limit,
      'current_page' => $json->page,
      'number_of_pages' => $json->pages,
      'search_stats' => $this->getSearchStats($json, $search_text),
      'publications' => $publications,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getCategoryOptions(): array {
    $cache_key = 'openwoo_search.category_options';

    if ($cache = $this->cache->get($cache_key)) {
      return $cache->data;
    }

    $headers = [
      'Accept' => 'application/json+aggregations',
    ];

    $query = [
      'organisatie.oin' => $this->getOin(),
      '_queries[]' => 'categorie',
    ];

    $options = $this->getOptions($query, $headers);
    $content = $this->sendRequest($this->getApiUrl(), $options);

    if ($content === NULL) {
      return [];
    }

    $content = json_decode($content);

    $categories = $content->categorie;

    $category_options = [];
    foreach ($categories as $category) {
      $category_id = $category->_id;
      $category_options[$category_id] = $category_id;
    }

    ksort($category_options);

    $this->storeInCache($cache_key, $category_options);
    return $category_options;
  }

  /**
   * Store data in cache for the given cache key.
   *
   * @param string $cache_key
   *   The cache key.
   * @param mixed $data
   *   The data that needs to be stored.
   */
  protected function storeInCache(string $cache_key, mixed $data): void {
    $this->cache->set(
      $cache_key,
      $data,
      $this->time->getRequestTime() + self::CACHE_EXPIRE,
      [
        'config:openwoo_search.settings',
      ]
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getyearOptions(): array {
    $cache_key = 'openwoo_search.year_options';

    if ($cache = $this->cache->get($cache_key)) {
      return $cache->data;
    }

    $query = [
      'organisatie.oin' => $this->getOin(),
      '_queries[]' => 'publicatiedatum',
    ];

    $headers = [
      'Accept' => 'application/json+aggregations',
    ];

    $options = $this->getOptions($query, $headers);
    $content = $this->sendRequest($this->getApiUrl(), $options);

    if ($content === NULL) {
      return [];
    }

    $content = json_decode($content);

    $years = $content->publicatiedatum;

    $year_options = [];
    foreach ($years as $year) {
      $id = substr($year->_id, 0, 4);
      $year_options[$id] = $id;
    }

    krsort($year_options);

    $this->storeInCache($cache_key, $year_options);
    return $year_options;
  }

  /**
   * Helper function to parse the date value.
   *
   * @param string $date
   *   The value to parse as iso date.
   *
   * @return string|null
   *   The iso date, defaults to an empty string if no valid date is found.
   */
  protected function getIsoDate(string $date): string|null {
    if (empty($date)) {
      return NULL;
    }

    return $this->dateFormatter->format(strtotime($date), 'custom', "Y-m-d\\TH:i:sO", NULL, 'nl');
  }

  /**
   * {@inheritDoc}
   */
  public function getElements(): array {
    $form['api_url'] = [
      '#type' => 'select',
      '#title' => $this->t('Api url'),
      '#empty_option' => $this->t('- Select -'),
      '#options' => $this->getApiUrlOptions(),
      '#description' => $this->t('Choose the URL you want to connect to. If unsure, consult OpenWoo.app.'),
      '#default_value' => $this->config()->get('api_url'),
    ];

    $api_key_description = $this->t('The API key is not required, but the API responses are faster when authenticated. <br> The API key must be saved in settings.php: <br> $config[&apos;openwoo_search.openwoo_app&apos;][&apos;api_key&apos;] = &apos;API_KEY&apos;;');
    $api_key_value = $this->configFactory->get($this->pluginDefinition['configName'])->get('api_key');
    $api_key = $api_key_value ? $this->maskApiKeyValue($api_key_value) : $this->t('Not set');

    $form['api_key'] = [
      '#type' => 'item',
      '#title' => $this->t('Api key'),
      '#plain_text' => $api_key,
      '#description' => $api_key_description,
    ];

    return $form;
  }

  /**
   * Get the api url options.
   *
   * @returns array
   *   the options array.
   */
  public function getApiUrlOptions(): array {
    $options = [
      'https://api.gateway.commonground.nu',
      'https://api.accept.common-gateway.commonground.nu',
      'https://api.common-gateway.commonground.nu',
    ];

    return array_combine($options, $options);
  }

  /**
   * {@inheritDoc}
   */
  public function submitElements(array $form, FormStateInterface $form_state) {
    $this->config()->set('api_url', $form_state->getValue('api_url'))->save();
  }

  /**
   * Mask api key value.
   *
   * @param string $api_key
   *   The api key.
   *
   * @return string
   *   The masked Api key.
   */
  protected function maskApiKeyValue(string $api_key): string {
    $mask = str_repeat('*', strlen($api_key) - 4);
    return $mask . substr($api_key, -4);
  }

  /**
   * Get the attachments of the OpenWoo publication.
   *
   * @param array $attachments
   *   Array with all the values of the publication.
   *
   * @return array
   *   The attachments array.
   */
  public function getAttachments(array $attachments): array {
    $attachments_array = [];
    foreach ($attachments as $attachment) {
      $attachments_array[] = [
        'file_name' => $attachment->titel,
        'link' => $attachment->url,
        'status' => $attachment->status,
      ];
    }
    return $attachments_array;
  }

}
