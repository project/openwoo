<?php

namespace Drupal\openwoo_search\Plugin\OpenWooSearch;

/**
 * Class representing a result in OpenWooSearch.
 */
class OpenWooSearchResult implements OpenWooSearchResultInterface {

  /**
   * The id.
   *
   * @var string
   */
  protected string $id;

  /**
   * The title.
   *
   * @var string
   */
  protected string $title;

  /**
   * The publish date.
   *
   * @var string
   */
  protected string $publishDate;

  /**
   * The publish date iso.
   *
   * @var string
   */
  protected string $publishDateIso;

  /**
   * The description.
   *
   * @var string
   */
  protected string $description;

  /**
   * The summary.
   *
   * @var string
   */
  protected string $summary;

  /**
   * The category.
   *
   * @var string
   */
  protected string $category;

  /**
   * The remote link.
   *
   * @var string
   */
  protected string $remoteLink;

  public function __construct() {
    $this->description = '';
    $this->summary = '';
  }

  /**
   * Set the id.
   *
   * @param string $id
   *   The id.
   */
  public function setId(string $id) {
    $this->id = $id;
  }

  /**
   * Get the id.
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * Set the title.
   *
   * @param string $title
   *   The title.
   */
  public function setTitle(string $title) {
    $this->title = $title;
  }

  /**
   * Get the title.
   */
  public function getTitle(): string {
    return $this->title;
  }

  /**
   * Set the publish date.
   *
   * @param string $publish_date
   *   The publish date.
   */
  public function setPublishDate(string $publish_date) {
    $this->publishDate = $publish_date;
  }

  /**
   * Get the publish date.
   */
  public function getPublishDate(): string {
    return $this->publishDate;
  }

  /**
   * Set the publish date iso.
   *
   * @param string $publish_date_iso
   *   The publish date iso.
   */
  public function setPublishDateIso(string $publish_date_iso) {
    $this->publishDateIso = $publish_date_iso;
  }

  /**
   * Get the publish date iso.
   */
  public function getPublishDateIso(): string {
    return $this->publishDateIso;
  }

  /**
   * Set the description.
   *
   * @param string $description
   *   The description.
   */
  public function setDescription(string $description) {
    $this->description = $description;
  }

  /**
   * Get the description.
   */
  public function getDescription(): string {
    return $this->description;
  }

  /**
   * Set the summary.
   *
   * @param string $summary
   *   The summary.
   */
  public function setSummary(string $summary) {
    $this->summary = $summary;
  }

  /**
   * Get the summary.
   */
  public function getSummary(): string {
    return $this->summary;
  }

  /**
   * Set the category.
   *
   * @param string $category
   *   The category.
   */
  public function setCategory(string $category) {
    $this->category = $category;
  }

  /**
   * Get the category.
   */
  public function getCategory(): string {
    return $this->category;
  }

  /**
   * Set the remote link.
   *
   * @param string $remote_link
   *   The remote link.
   */
  public function setRemoteLink(string $remote_link) {
    $this->remoteLink = $remote_link;
  }

  /**
   * Get the remote link.
   */
  public function getRemoteLink(): string {
    return $this->remoteLink;
  }

}
