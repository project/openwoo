<?php

namespace Drupal\openwoo_search\Plugin\OpenWooSearch;

/**
 * Interface for OpenWooSearch results.
 */
interface OpenWooSearchResultInterface {

  /**
   * Set the id of the result.
   *
   * @param string $id
   *   The title.
   */
  public function setId(string $id);

  /**
   * Get the id of the result.
   *
   * @return string
   *   The title.
   */
  public function getId(): string;

  /**
   * Set the title of the result.
   *
   * @param string $title
   *   The title.
   */
  public function setTitle(string $title);

  /**
   * Get the title of the result.
   *
   * @return string
   *   The title.
   */
  public function getTitle(): string;

  /**
   * Set the description of the result.
   *
   * @param string $description
   *   The title.
   */
  public function setDescription(string $description);

  /**
   * Get the description of the result.
   *
   * @return string
   *   The title.
   */
  public function getDescription(): string;

  /**
   * Set the summary of the result.
   *
   * @param string $summary
   *   The title.
   */
  public function setSummary(string $summary);

  /**
   * Get the summary of the result.
   *
   * @return string
   *   The title.
   */
  public function getSummary(): string;

  /**
   * Set the category of the result.
   *
   * @param string $category
   *   The title.
   */
  public function setCategory(string $category);

  /**
   * Get the category of the result.
   *
   * @return string
   *   The title.
   */
  public function getCategory(): string;

  /**
   * Set the publish date of the result.
   *
   * @param string $publish_date
   *   The title.
   */
  public function setPublishDate(string $publish_date);

  /**
   * Get the iso publish date of the result.
   *
   * @return string
   *   The title.
   */
  public function getPublishDateIso(): string;

  /**
   * Set the iso publish date of the result.
   *
   * @param string $publish_date_iso
   *   The title.
   */
  public function setPublishDateIso(string $publish_date_iso);

  /**
   * Get the publish date of the result.
   *
   * @return string
   *   The title.
   */
  public function getPublishDate(): string;

  /**
   * Set the remote link of the result.
   *
   * @param string $remote_link
   *   The title.
   */
  public function setRemoteLink(string $remote_link);

  /**
   * Get the remote link of the result.
   *
   * @return string
   *   The title.
   */
  public function getRemoteLink(): string;

}
