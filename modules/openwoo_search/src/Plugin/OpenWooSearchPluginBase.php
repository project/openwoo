<?php

namespace Drupal\openwoo_search\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\PluginWithFormsTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\openwoo\OpenWooPublicationFields;
use Drupal\openwoo_search\OpenWooSearchPluginManager;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for OpenWooSearch plugins.
 */
abstract class OpenWooSearchPluginBase extends PluginBase implements OpenWooSearchPluginInterface {

  use PluginWithFormsTrait;
  use StringTranslationTrait;
  use LoggerChannelTrait;

  protected const GET = 'get';
  protected const POST = 'post';
  protected const PUT = 'put';

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * A http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The plugin manager for 'OpenWoo search'.
   *
   * @var \Drupal\openwoo_search\OpenWooSearchPluginManager
   */
  protected $openWooSearchPluginManager;

  /**
   * Service with helpers for the form fields.
   *
   * @var \Drupal\openwoo\OpenWooPublicationFields
   */
  protected $publicationFieldsService;

  /**
   * Constructs a Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The http client.
   * @param \Drupal\openwoo_search\OpenWooSearchPluginManager $openwoo_search_plugin_manager
   *   The OpenWoo plugin manager.
   * @param \Drupal\openwoo\OpenWooPublicationFields $publication_fields_service
   *   The OpenWooPublicationFields service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactoryInterface $config_factory,
    ClientInterface $http_client,
    OpenWooSearchPluginManager $openwoo_search_plugin_manager,
    OpenWooPublicationFields $publication_fields_service,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
    $this->openWooSearchPluginManager = $openwoo_search_plugin_manager;
    $this->publicationFieldsService = $publication_fields_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('http_client'),
      $container->get('plugin.manager.openwoo_search'),
      $container->get('openwoo.publication_fields')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function description(): string {
    return (string) $this->pluginDefinition['description'];
  }

  /**
   * Get the configuration object.
   *
   * @return \Drupal\Core\Config\Config
   *   The configuration object.
   */
  protected function config(): Config {
    return $this->configFactory->getEditable($this->pluginDefinition['configName']);
  }

  /**
   * Send the request to the api.
   *
   * @param string $uri
   *   The uri to send the request to.
   * @param array $options
   *   The options for the request, e.g. the request body.
   * @param string $method
   *   The request method, e.g. 'get' or 'post', defaults to 'get'.
   *
   * @return string|null
   *   The response object or null if no response found.
   *
   * @throws \GuzzleHttp\Exception\ClientException
   *   The client exception from the REST call.
   */
  protected function sendRequest(string $uri, array $options = [], string $method = self::GET): string|NULL {
    try {
      $result = $this->httpClient->request($method, $uri, $options);
      if ($result->getStatusCode() === 200 || $result->getStatusCode() === 201) {
        return $result->getBody()->getContents();
      }
      else {
        $this->getLogger('openwoo_search')
          ->error('Could not connect to OpenWoo client. We received the following status code: @status_code', ['@status_code' => $result->getStatusCode()]);
        return NULL;
      }
    }
    catch (RequestException  $exception) {
      $this->getLogger('openwoo_search')
        ->error('Could not connect to OpenWoo client: @message', ['@message' => $exception->getMessage()]);
      return NULL;
    }
  }

  /**
   * Get the oin.
   *
   * @return string
   *   The oin.
   */
  protected function getOin(): string {
    return $this->configFactory->get('openwoo.settings')->get('oin');
  }

  /**
   * Get the amount of items per page.
   *
   * @return int
   *   The amount of items per page.
   */
  public function getItemsPerPage(): int {
    return $this->configFactory->get('openwoo_search.settings')->get('items_per_page');
  }

}
