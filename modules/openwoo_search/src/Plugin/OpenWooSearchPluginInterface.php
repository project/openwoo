<?php

namespace Drupal\openwoo_search\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Interface for OpenWooSearch plugins.
 */
interface OpenWooSearchPluginInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label(): string;

  /**
   * Returns the translated plugin description.
   *
   * @return string
   *   The translated title.
   */
  public function description(): string;

  /**
   * Get the search results for a search query.
   *
   * @param string|null $search_text
   *   The search text.
   * @param string|null $year
   *   The year value.
   * @param string|null $category
   *   The category value.
   * @param int|null $page
   *   The page to retrieve.
   *
   * @return array|null
   *   The json.
   */
  public function search(string $search_text = NULL, string $year = NULL, string $category = NULL, int $page = NULL): array|null;

  /**
   * Get the details of the publication.
   *
   * @param string $external_publication_id
   *   The external publication id.
   *
   * @return array
   *   The publication values.
   */
  public function getDetails(string $external_publication_id): array|null;

  /**
   * Get the search statistics.
   *
   * @param object $json
   *   The search json.
   * @param string $search_text
   *   The search text.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The statistics.
   */
  public function getSearchStats(object $json, string $search_text): TranslatableMarkup;

  /**
   * Parse the search json.
   *
   * @param object $json
   *   The json that needs to be parsed.
   * @param string $search_text
   *   The search text.
   *
   * @return array
   *   The search result.
   */
  public function parseResults(object $json, string $search_text = NULL): array;

  /**
   * Get the category options.
   *
   * @return array|null
   *   The json.
   */
  public function getCategoryOptions(): array|null;

  /**
   * Get the year options.
   *
   * @return array|null
   *   The json.
   */
  public function getyearOptions(): array|null;

  /**
   * Build the form elements for the settings form.
   *
   * @return array
   *   A list of elements to show on the form.
   */
  public function getElements(): array;

  /**
   * Process the submitted form values.
   *
   * This function is called when the parent form is submitted.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function submitElements(array $form, FormStateInterface $form_state);

}
