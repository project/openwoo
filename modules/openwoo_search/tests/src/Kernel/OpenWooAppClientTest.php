<?php

namespace Drupal\Tests\openwoo\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\openwoo_search\Plugin\OpenWooSearch\OpenWooApp;
use Drupal\Tests\user\Traits\UserCreationTrait;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * Tests the OpenWoo settings route access.
 *
 * @group openwoo
 */
class OpenWooAppClientTest extends KernelTestBase {
  use UserCreationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'user',
    'system',
    'openwoo',
    'openwoo_search',
  ];

  /**
   * The Importer we will test.
   *
   * @var \Drupal\openwoo_search\Plugin\OpenWooSearch\OpenWooApp
   */
  protected OpenWooApp $openWooAppClient;

  /**
   * Tests search results.
   */
  public function testGetSearchResults(): void {
    $this->initiateOpenWooSearchClient(new Response(200, [], $this->getDummyResponse('OpenWooAppClientResponse.json')));

    $response = $this->openWooAppClient->search('testText');
    $this->assertEquals(18, $response['items']);
    $this->assertEquals('Result 1 - 18 of 18 results for search term: testText', $response['search_stats']->render());

    $publication = $response['publications'][0];
    // cspell:disable
    $this->assertEquals('73116b9d-b517-4815-8ac0-2eebf1a4c158', $publication->getId());
    $this->assertEquals('Onderzoek damwandconstructie kademuur rivier de Nieuwe Maas', $publication->getTitle());
    $this->assertEquals('test description', $publication->getDescription());
    $this->assertEquals('test summary', $publication->getSummary());
    $this->assertEquals('Woo-verzoeken en -besluiten', $publication->getCategory());
    $this->assertEquals('2023-11-14T15:34:43+00:00', $publication->getPublishDate());
    $this->assertEquals('/openwoo-search/result/73116b9d-b517-4815-8ac0-2eebf1a4c158', $publication->getRemoteLink());
    // cspell:enable
  }

  /**
   * Returns a valid response object.
   *
   * @param string $filename
   *   The filename of the stub to load.
   * @param int $status
   *   The HTTP status code (optional).
   *
   * @return string
   *   The response object.
   */
  private function getDummyResponse(string $filename, int $status = 200): string {
    $module_path = \Drupal::service('module_handler')
      ->getModule('openwoo_search')
      ->getPath();
    return file_get_contents($module_path . '/tests/src/Stubs/' . $filename);
  }

  /**
   * Initiates the openWooSearchClient with the expected server response.
   *
   * @param \GuzzleHttp\Psr7\Response $expected_response
   *   The expected response.
   */
  private function initiateOpenWooSearchClient(Response $expected_response): void {
    $configuration = [];
    $plugin_id = 'openwoo_app';
    $plugin_definition = [
      'id' => 'openwoo_app',
      'label' => 'OpenWoo.app',
      'description' => 'Search openwoo publications in OpenWoo.app',
      'configName' => 'openwoo_search.openwoo_app',
      'class' => 'Drupal\openwoo_search\Plugin\OpenWooSearch\OpenWooApp',
      'provider' => 'openwoo_search',
    ];

    $config_factory = \Drupal::configFactory();
    $openwoo_Search_config = $config_factory->getEditable('openwoo_search.settings');
    $openwoo_Search_config->set('client', 'openwoo_app');
    $openwoo_Search_config->set('items_per_page', 50);
    $openwoo_Search_config->save(TRUE);
    $openwoo_config = $config_factory->getEditable('openwoo.settings');
    $openwoo_config->set('oin', '00000001001877410000');
    $openwoo_config->save(TRUE);

    $mock = new MockHandler([$expected_response]);
    $handlerStack = HandlerStack::create($mock);
    $http_client = new Client(['handler' => $handlerStack]);
    $date_formatter = $this->container->get('date.formatter');
    $openwoo_search_plugin_manager = $this->container->get('plugin.manager.openwoo_search');

    $this->openWooAppClient = new OpenWooApp(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $config_factory,
      $http_client,
      $openwoo_search_plugin_manager,
      $date_formatter,
      $this->container->get('cache.default'),
      $this->container->get('datetime.time'),
      $this->container->get('openwoo.publication_fields')
    );
  }

}
