<?php

namespace Drupal\Tests\openwoo_search\Kernel;

use Drupal\Core\Url;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Tests the OpenWoo settings route access.
 *
 * @group openwoo
 */
class SearchSettingsFormAccessTest extends KernelTestBase {
  use UserCreationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'user',
    'system',
    'openwoo',
    'openwoo_search',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
  }

  /**
   * Tests the settings route access.
   */
  public function testSettingsRouteAccess() {
    $user_with_access = $this->setUpCurrentUser([], ['administer openwoo configuration']);
    $user_without_access = $this->setUpCurrentUser([], []);

    $url = Url::fromRoute('openwoo_search.settings');
    $access = $url->access($user_with_access);
    $this->assertTrue($access, 'Admin user has no access to the settings route.');

    $access = $url->access($user_without_access);
    $this->assertFalse($access, 'Non-admin user should not have access to the settings route.');
  }

}
