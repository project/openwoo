<?php

/**
 * @file
 * API documentation for the OpenWoo module.
 */

/**
 * Alter the list with OpenWoo publication categories.
 */
function hook_openwoo_publication_categories_alter(&$categories) {
  // Add a custom category.
  $categories['Omgevingsplan'] = 'Omgevingsplan';
}
