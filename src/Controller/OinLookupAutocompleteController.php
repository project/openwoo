<?php

namespace Drupal\openwoo\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a route controller for OpenWoo Oin Lookup autocomplete form elements.
 *
 * @see \Drupal\openwoo\Plugin\Field\FieldWidget\OinLookupDefaultWidget
 */
class OinLookupAutocompleteController extends ControllerBase {
  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  const OINREGISTER_API_URL = 'https://oinregister.logius.nl/api/v1/organisaties?fields=oin,naam,organisatieCode,organisatieType&organisatieType=GM';

  /**
   * {@inheritdoc}
   */
  public function __construct(ClientInterface $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client')
    );
  }

  /**
   * Handler for autocomplete request.
   */
  public function handleAutocomplete(Request $request) {
    $results = [];
    $input = $request->query->get('q');

    if (!$input) {
      return new JsonResponse($results);
    }

    $input = Xss::filter($input);
    $url = sprintf(self::OINREGISTER_API_URL . "&naam=%s", $input);
    $response = $this->httpClient->get($url);

    if ($response->getStatusCode() !== 200) {
      return new JsonResponse($results);
    }

    $response = json_decode($response->getBody()->getContents(), TRUE);

    foreach ($response['organisaties'] as $organization) {

      // Skip malformed.
      if (!isset($organization['oin'], $organization['naam'], $organization['organisatieCode'])) {
        continue;
      }

      $results[] = [
        'value' => sprintf('%s (%s|%s)', $organization['naam'], $organization['organisatieCode'], $organization['oin']),
        'label' => sprintf('%s (%s|%s)', $organization['naam'], $organization['organisatieCode'], $organization['oin']),
      ];
    }

    return new JsonResponse($results);
  }

}
