<?php

namespace Drupal\openwoo\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class OpenWooSettingsForm. The config form for the OpenWoo module.
 *
 * @package Drupal\openwoo\Form
 */
class OpenWooSettingsForm extends ConfigFormBase {

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return [
      'openwoo.settings',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'openwoo_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('openwoo.settings');

    $prefill_details_open = TRUE;
    $prefill_description = $this->t('Use the field below to select an organization.
    Begin typing the name of your organization, f.e. "Breda", and if you see the right
    suggestion appear, select it.');
    $manual_description = $this->t('Use the fields below to enter the data for
     an organization manually. Note that if an organization is selected in the search field
     this will overrule the settings in de fields below.');
    if (!empty($config->get('name') && !empty($config->get('oin')) && !empty($config->get('tooi')))) {
      $prefill_details_open = FALSE;
      $prefill_description = $this->t('Currently selected @name. Use the prefill only
      if you want to change this. The prefill will overwrite the fields below.', [
        '@name' => $config->get('name'),
      ]);
      $manual_description = $this->t('These are the current settings. You can change
      them, or use the "Search organization" option above to select another organization.');
    }

    $form['organization'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Organization'),
    ];

    $form['organization']['prefill'] = [
      '#type' => 'details',
      '#title' => $this->t('Search organization'),
      '#open' => $prefill_details_open,
    ];

    $form['organization']['prefill']['divider1'] = [
      '#markup' => '<p>' . $prefill_description . '</p>',
    ];

    $form['organization']['prefill']['oin_prefill'] = [
      '#title' => $this->t('Find organization'),
      '#type' => 'textfield',
      '#autocomplete_route_name' => 'openwoo.oin_lookup.autocomplete',
      '#autocomplete_route_parameters' => [],
      '#placeholder' => t('Municipality'),
    ];

    $form['organization']['manual'] = [
      '#type' => 'details',
      '#title' => $this->t('Manual'),
      '#open' => !$prefill_details_open,
    ];

    $form['organization']['manual']['divider2'] = [
      '#markup' => '<p>' . $manual_description . '</p>',
    ];

    $form['organization']['manual']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#description' => $this->t('Name of the organization.'),
      '#default_value' => $config->get('name'),
    ];
    $form['organization']['manual']['oin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Oin'),
      '#description' => $this->t('Oin of the organization e.g. 00000001001902921000. For the full list see: https://oinregister.logius.nl/oin-register'),
      '#default_value' => $config->get('oin'),
    ];
    $form['organization']['manual']['tooi'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tooi'),
      '#default_value' => $config->get('tooi'),
      '#description' => $this->t('Tooi of the organization f.e. GM_0848.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('oin_prefill')) &&
      (empty($form_state->getValue('name')) || empty($form_state->getValue('oin')) || empty($form_state->getValue('tooi')))) {
      $form_state->setErrorByName(
        'oin_prefill',
        $this->t('You must use either the prefill or enter all fields manually below.'),
      );
    }
    if (!empty($form_state->getValue('oin_prefill')) && empty($this->massagePrefilledData($form, $form_state))) {
      $form_state->setErrorByName(
        'oin_prefill',
        $this->t('The prefilled data is not valid. Please try again, or enter the data manually below.'),
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Text input fields.
    $name = $form_state->getValue('name');
    $oin = $form_state->getValue('oin');
    $tooi = $form_state->getValue('tooi');

    // Autocomplete overrules text input.
    $prefilled_data = $this->massagePrefilledData($form, $form_state);
    if (!empty($prefilled_data)) {
      $name = $prefilled_data['name'];
      $oin = $prefilled_data['oin'];
      $tooi = 'GM_' . $prefilled_data['tooi'];
    }
    $this->config('openwoo.settings')
      ->set('name', $name)
      ->set('oin', $oin)
      ->set('tooi', $tooi)
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * We process the autocomplete field.
   *
   * @param array $form
   *   The submitted form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form_state.
   *
   * @return array
   *   Return an array with name, oin and tooi.
   *   If the field is not valid, return empty array.
   */
  private function massagePrefilledData(array &$form, FormStateInterface $form_state):array {
    // Take "Name (Tooi|Oin)", get the value inside the parentheses.
    if (preg_match('/(.+\\s)\\(([^\\)]+)\\)/', $form_state->getValue('oin_prefill'), $matches)) {
      $item['name'] = trim($matches[1]);
      $tooi_oin = explode('|', $matches[2]);
      $item['tooi'] = preg_replace("/[^0-9\s]/", "", trim($tooi_oin[0]));
      $item['oin'] = preg_replace("/[^0-9\s]/", "", trim($tooi_oin[1]));
    }
    if (!empty($item['name']) && !empty($item['tooi']) && !empty($item['oin'])) {
      return $item;
    }
    else {
      return [];
    }
  }

}
