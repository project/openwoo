<?php

namespace Drupal\openwoo;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provide form fields and supporting functions.
 */
class OpenWooPublicationFields {

  use StringTranslationTrait;

  /**
   * Config factory to get OpenWoo config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The module handler.
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Array with the form fields.
   *
   * @var array
   */
  protected array $formFields;

  /**
   * Array with container field types.
   *
   * @var array
   */
  protected array $containerFieldTypes;

  /**
   * Array with the different request types available in the publication.
   *
   * @var array
   */
  protected array $requestTypes;

  /**
   * Array with the publication data.
   *
   * @var array
   */
  protected array $publicationData;

  /**
   * Constructs a new OpenWoo Publication fields object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler) {
    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
    $this->containerFieldTypes = ['details', 'fieldset'];
    $this->requestTypes = [
      'decree' => $this->t('Decree'),
      'information_request' => $this->t('Information request'),
      'inventory_list' => $this->t('Inventory list'),
    ];
  }

  /**
   * Return an array with all form fields and their properties.
   *
   * In this function we define all the fields that make up the publication.
   * Add fields here to extend the publication. They will be saved in the
   * data-field as serialized data.
   *
   * @return array
   *   Array with form fields.
   */
  public function getFormFields(): array {
    $this->formFields = [
      'general' => [
        'type' => 'fieldset',
        'label' => $this->t('General'),
        'elements' => [
          'publication_date' => [
            'type' => 'date',
            'label' => $this->t('Publication date'),
            'description' => $this->t('The date this publication will be published.
            If in the future, the publication will be unpublished until this date.'),
            'required' => TRUE,
          ],
          'category' => [
            'type' => 'select',
            'label' => $this->t('Category'),
            'description' => $this->t('Choose the suitable category from the pull-down.'),
            'required' => TRUE,
            'options' => $this->getCategoryList(),
          ],
          'summary' => [
            'type' => 'textfield',
            'label' => $this->t('Summary'),
            'description' => $this->t('A short summary of the publication.'),
          ],
          'description' => [
            'type' => 'textfield',
            'label' => $this->t('Description'),
            'description' => $this->t('A brief description of the publication.'),
          ],
        ],
      ],
      'theme' => [
        'type' => 'fieldset',
        'label' => $this->t('Theme'),
        'elements' => [
          'main_theme' => [
            'type' => 'textfield',
            'label' => $this->t('Main theme'),
            'description' => $this->t('What is the main theme of the request?'),
            'required' => TRUE,
          ],
          'sub_theme' => [
            'type' => 'textfield',
            'label' => $this->t('Sub theme'),
            'description' => $this->t('Optionally add a sub theme.'),
            'required' => FALSE,
          ],
          'additional_theme' => [
            'type' => 'textfield',
            'label' => $this->t('Additional theme'),
            'description' => $this->t('Optionally add an additional theme.'),
            'required' => FALSE,
          ],
        ],
      ],
      'metadata' => [
        'type' => 'fieldset',
        'label' => $this->t('Metadata'),
        'elements' => [
          'metadata_serialnumber' => [
            'type' => 'textfield',
            'description' => $this->t('The number the request is recorded at in the local system.'),
            'label' => $this->t('Serial number'),
          ],
          'metadata_decision_date' => [
            'type' => 'date',
            'label' => $this->t('Decision date'),
            'description' => $this->t('Date for the decision.'),
          ],
          'request' => [
            'type' => 'fieldset',
            'label' => $this->t('Request'),
            'elements' => [
              'request_status' => [
                'type' => 'textfield',
                'label' => $this->t('Status'),
                'description' => $this->t('Current status of this request.'),
              ],
              'request_recipient' => [
                'type' => 'textfield',
                'label' => $this->t('Recipient for this request'),
                'description' => $this->t('At which organization or department the request was filed.'),
              ],
              'request_receipt_date' => [
                'type' => 'date',
                'label' => $this->t('Receipt date'),
                'description' => $this->t('The date the request was received.'),
              ],
              'request_term_exceeded' => [
                'type' => 'textfield',
                'label' => $this->t('Term exceeded'),
                'description' => $this->t('Is the response term exceeded?'),
              ],
            ],
          ],
        ],
      ],
    ];
    $this->addRequestFields();
    $config = $this->configFactory->get('openwoo.settings');
    $this->formFields['organization'] = [
      'type' => 'fieldset',
      'label' => $this->t('Organization'),
      'elements' => [
        'organization_name' => [
          'type' => 'textfield',
          'label' => $this->t('Name'),
          'required' => TRUE,
          'default_value' => $config->get('name'),
        ],
        'organization_oin' => [
          'type' => 'textfield',
          'label' => $this->t('Oin'),
          'default_value' => $config->get('oin'),
        ],
        'organization_tooi' => [
          'type' => 'textfield',
          'label' => $this->t('Tooi'),
          'default_value' => $config->get('tooi'),
        ],
        'organization_rsin' => [
          'type' => 'textfield',
          'label' => $this->t('Rsin'),
        ],
      ],
    ];
    return $this->formFields;
  }

  /**
   * Function generates the request field names.
   *
   * Request types are defined in the constructor.
   * Each type has the same fields, so we generate them for each type.
   * This function is used in form validation.
   *
   * @return array
   *   Return an array with all request fields as value.
   */
  public function getRequestFields(): array {
    $all_request_types = [];
    foreach ($this->requestTypes as $key => $value) {
      $all_request_types[$key]['label'] = $value;
      $all_request_types[$key]['fields'] = [
        $key . '_title',
        $key . '_url',
        $key . '_status',
        $key . '_type',
        $key . '_category',
        $key . '_extension',
      ];
    }
    return $all_request_types;
  }

  /**
   * Function adds the form field array for Request types.
   *
   * There are three request types: decree, information request and
   * inventory list. Each type has the same fields, so we generate
   * them for each type.
   */
  private function addRequestFields(): void {
    foreach ($this->requestTypes as $key => $value) {
      $readable_request_type = strtolower($this->requestTypes[$key]);
      $this->formFields['metadata']['elements']['request']['elements'][$key] = [
        'type' => 'details',
        'label' => $value,
        'elements' => [
          $key . '_title' => [
            'type' => 'textfield',
            'label' => $this->t('Title'),
            'description' => $this->t('The title of this @request_type.', ['@request_type' => $readable_request_type]),
          ],
          $key . '_url' => [
            'type' => 'textfield',
            'label' => $this->t('URL'),
            'description' => $this->t('The URL where this @request_type can be downloaded.', ['@request_type' => $readable_request_type]),
          ],
          $key . '_status' => [
            'type' => 'textfield',
            'label' => $this->t('Status'),
            'description' => $this->t('The current status of this @request_type.', ['@request_type' => $readable_request_type]),
          ],
          $key . '_type' => [
            'type' => 'textfield',
            'label' => $this->t('Type'),
            'description' => $this->t('The type of this @request_type.', ['@request_type' => $readable_request_type]),
          ],
          $key . '_category' => [
            'type' => 'textfield',
            'label' => $this->t('Category'),
            'description' => $this->t('The category of this @request_type.', ['@request_type' => $readable_request_type]),
          ],
          $key . '_extension' => [
            'type' => 'textfield',
            'label' => $this->t('Extension'),
            'description' => $this->t('The file extension of the document for this @request_type.', ['@request_type' => $readable_request_type]),
          ],
        ],
      ];
    }
  }

  /**
   * Return an array with options for subjects.
   *
   * Source https://www.open-overheid.nl/instrumenten-en-diensten/publicaties/2023/07/27/verkorte-namen-woo-informatiecategorieen.
   * This is only in Dutch, so no translation is added.
   *
   * @return array
   *   Array with options.
   */
  public function getCategoryList(): array {
    $categories = [
      "" => $this->t('-- Choose an option --'),
      "3_3_2e" => "Adviezen",
      "3_3_2d" => "Agenda's en besluitenlijsten bestuurscolleges",
      "3_3_1e" => "Bereikbaarheidsgegevens",
      "3_3_2k" => "Beschikkingen",
      "3_3_2a" => "Bij vertegenwoordigende organen ingekomen stukken",
      "3_3_2f" => "Convenanten",
      "3_3_2g" => "Jaarplannen en jaarverslagen",
      "3_3_2l" => "Klachtoordelen",
      "3_3_2j" => "Onderzoeksrapporten",
      "3_3_1c" => "Ontwerpen van wet- en regelgeving met adviesaanvraag",
      "3_3_1d" => "Organisatie en werkwijze",
      "3_3_1b" => "Overige besluiten van algemene strekking",
      "3_3_2h" => "Subsidieverplichtingen anders dan met beschikking",
      "3_3_2c" => "Vergaderstukken decentrale overheden",
      "3_3_2b" => "Vergaderstukken Staten-Generaal",
      "3_3_1a" => "Wetten en algemeen verbindende voorschriften",
      "3_3_2i" => "Woo-verzoeken en -besluiten",
    ];

    // Allow other modules to alter the category list.
    $this->moduleHandler->alter('openwoo_publication_categories', $categories);

    return $categories;
  }

  /**
   * Generate the create/edit form for the OpenWoo publication.
   *
   * @param array $form_values
   *   Array with all the values of the form.
   *
   * @return array
   *   Render array for form with added fields.
   */
  public function getForm(array $form_values): array {
    $this->publicationData = $form_values;
    return $this->generatePublication($this->getFormFields(), 'form');
  }

  /**
   * Generate the page display for the OpenWoo publication.
   *
   * @param array $publication_values
   *   Array with all the values of the publication.
   *
   * @return array
   *   Render array for form with added fields.
   */
  public function getPublication(array $publication_values): array {
    $this->publicationData = $publication_values;
    return $this->generatePublication($this->getFormFields(), 'view');
  }

  /**
   * Generate the render array for the publication.
   *
   * This function generates either the render array with form-elements
   * or with view-elements.
   *
   * @param array $elements
   *   Array with all form fields that must be generated.
   * @param string $view_mode
   *   String "view" or "form" what type of element should be returned.
   *
   * @return array
   *   Resulted render array.
   */
  public function generatePublication(array $elements, string $view_mode): array {
    $form_array = [];
    foreach ($elements as $key => $element) {
      $element_required = $element['required'] ?? FALSE;
      $element_options = $element['options'] ?? [];
      $element_default_value = $element['default_value'] ?? '';
      $element_description = $element['description'] ?? '';
      $recursive_elements = [];
      if (in_array($element['type'], $this->containerFieldTypes)) {
        $recursive_elements = $this->generatePublication($element['elements'], $view_mode);
      }
      if ($view_mode === 'view') {
        if (array_key_exists($key, $this->requestTypes) && !$this->requestTypeIsFilled($key)) {
          // Only render a request container if the request is filled.
          continue;
        }
        $form_array[$key] = $this->addViewField($key, $element['type'], $element['label']);
        // Add child elements recursively.
        $form_array[$key]['#item_elements'] = $recursive_elements;
      }
      else {
        $form_array[$key] = $this->addFormField($key, $element['type'], $element['label'], $element_required, $element_options, $element_default_value, $element_description);
        // Add child elements recursively.
        $form_array[$key] += $recursive_elements;
      }
    }
    return $form_array;
  }

  /**
   * Return a form render array item for an element.
   *
   * @param string $name
   *   The name of the element.
   * @param string $type
   *   The element type.
   * @param string $title
   *   The title of the element.
   * @param bool $required
   *   Optional if the element is required.
   * @param array $options
   *   Optional options for the option list.
   * @param string $default_value
   *   Optional default value other than ''.
   * @param string $description
   *   Optional description other than ''.
   *
   * @return array
   *   Render array of a form element.
   */
  public function addFormField(string $name, string $type, string $title, bool $required = FALSE, array $options = [], string $default_value = '', string $description = ''): array {
    $default_value = empty($default_value) ? '' : $default_value;

    $element = [
      '#type' => $type,
      '#title' => $title,
    ];
    if ($description) {
      $element['#description'] = $description;
    }
    if (!in_array($type, $this->containerFieldTypes)) {
      $element['#default_value'] = $this->publicationData[$name] ?? $default_value;
    }
    if ($required) {
      $element['#required'] = TRUE;
    }
    if ($type === 'select') {
      $element['#options'] = $options;
    }
    return $element;
  }

  /**
   * Return a render array to display an element on a page.
   *
   * @param string $name
   *   The name of the element.
   * @param string $type
   *   The element type.
   * @param string $title
   *   The title of the element.
   *
   * @return array
   *   Render array of a page element.
   */
  public function addViewField(string $name, string $type, string $title): array {
    if (!in_array($type, $this->containerFieldTypes)) {
      // This is an item, get value.
      $value = $this->publicationData[$name] ?? '';
      if (empty($value)) {
        return [];
      }
      // Get readable value for category-select.
      $category_list = $this->getCategoryList();
      if ($name === 'category') {
        $value = $category_list[$value];
      }
      return [
        '#theme' => 'openwoo_publish_item',
        '#item_name' => $name,
        '#item_type' => $type,
        '#item_title' => $title,
        '#item_value' => $value ?? '',
      ];
    }
    else {
      // This is a container.
      return [
        '#theme' => 'openwoo_publish_container',
        '#item_name' => $name,
        '#item_type' => $type,
        '#item_title' => $title,
      ];
    }
  }

  /**
   * Function checks if the request type is filled.
   *
   * The title field is required. So we check if this field is
   * filled, then the request is filled.
   *
   * @param string $type_to_check
   *   The request type to check.
   *
   * @return bool
   *   TRUE if the request type is filled.
   */
  public function requestTypeIsFilled(string $type_to_check): bool {
    return !empty($this->publicationData[$type_to_check . '_title']);
  }

}
